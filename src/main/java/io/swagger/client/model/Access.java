package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.AccessFieldsMap;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class Access   {
  
  private Long id = null;
  private Long providerId = null;
  private AccessFieldsMap accessFields = null;


  public enum ValidationStateEnum {
    OK("OK"),
    ACCESS_LOCKED("ACCESS_LOCKED"),
    ACCESS_WRONG("ACCESS_WRONG");

    private String value;

    ValidationStateEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return value;
    }
  }

  private ValidationStateEnum validationState = null;
  private String type = null;

  
  /**
   * Internal ID of this access (generated value)
   **/
  
  @ApiModelProperty(value = "Internal ID of this access (generated value)")
  @JsonProperty("id")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  
  /**
   * Identifier of the provider to which this access belongs
   **/
  
  @ApiModelProperty(required = true, value = "Identifier of the provider to which this access belongs")
  @JsonProperty("providerId")
  public Long getProviderId() {
    return providerId;
  }
  public void setProviderId(Long providerId) {
    this.providerId = providerId;
  }

  
  /**
   * Authentication data for the given provider as map of InputFieldId to String (e.g., `{\"USERNAME\", \"yourName\"}`)\n The fields necessary for the access are determined by the \n InputField descriptions of the related provider.
   **/
  
  @ApiModelProperty(required = true, value = "Authentication data for the given provider as map of InputFieldId to String (e.g., `{\"USERNAME\", \"yourName\"}`)\n The fields necessary for the access are determined by the \n InputField descriptions of the related provider.")
  @JsonProperty("accessFields")
  public AccessFieldsMap getAccessFields() {
    return accessFields;
  }
  public void setAccessFields(AccessFieldsMap accessFields) {
    this.accessFields = accessFields;
  }

  
  /**
   * The state reflects the validity of the access credentials. The state can change after communicating with the provider.\n It can be OK (access credentials are valid); `ACCESS_LOCKED` (access is locked: This can happen when, for example, someone tried to login to your account by\n using an incorrect PIN too many times or if your account was used for illegal purposes &mdash; automatic refresh will be disabled); or `ACCESS_WRONG` (access wrong: Saved\n credentials are incorrect and no communication with the provider is possible &mdash; automatic refresh will be disabled)
   **/
  
  @ApiModelProperty(value = "The state reflects the validity of the access credentials. The state can change after communicating with the provider.\n It can be OK (access credentials are valid); `ACCESS_LOCKED` (access is locked: This can happen when, for example, someone tried to login to your account by\n using an incorrect PIN too many times or if your account was used for illegal purposes &mdash; automatic refresh will be disabled); or `ACCESS_WRONG` (access wrong: Saved\n credentials are incorrect and no communication with the provider is possible &mdash; automatic refresh will be disabled)")
  @JsonProperty("validationState")
  public ValidationStateEnum getValidationState() {
    return validationState;
  }
  public void setValidationState(ValidationStateEnum validationState) {
    this.validationState = validationState;
  }

  
  /**
   * Discriminator for subtypes. At the moment only `BankAccess` is supported.
   **/
  
  @ApiModelProperty(required = true, value = "Discriminator for subtypes. At the moment only `BankAccess` is supported.")
  @JsonProperty("type")
  public String getType() {
    return type;
  }
  public void setType(String type) {
    this.type = type;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Access access = (Access) o;
    return Objects.equals(id, access.id) &&
        Objects.equals(providerId, access.providerId) &&
        Objects.equals(accessFields, access.accessFields) &&
        Objects.equals(validationState, access.validationState) &&
        Objects.equals(type, access.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, providerId, accessFields, validationState, type);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Access {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    providerId: ").append(toIndentedString(providerId)).append("\n");
    sb.append("    accessFields: ").append(toIndentedString(accessFields)).append("\n");
    sb.append("    validationState: ").append(toIndentedString(validationState)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

