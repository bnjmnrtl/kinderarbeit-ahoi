package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.Balance;
import io.swagger.client.model.TransactionPattern;
import java.util.*;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class Forecast   {
  
  private Balance forecastBalance = null;
  private Long accountId = null;
  private List<TransactionPattern> unappliedTransactionPatterns = new ArrayList<TransactionPattern>();

  
  /**
   * Balance forecast
   **/
  
  @ApiModelProperty(required = true, value = "Balance forecast")
  @JsonProperty("forecastBalance")
  public Balance getForecastBalance() {
    return forecastBalance;
  }
  public void setForecastBalance(Balance forecastBalance) {
    this.forecastBalance = forecastBalance;
  }

  
  /**
   * ID of account to which this entry belongs
   **/
  
  @ApiModelProperty(required = true, value = "ID of account to which this entry belongs")
  @JsonProperty("accountId")
  public Long getAccountId() {
    return accountId;
  }
  public void setAccountId(Long accountId) {
    this.accountId = accountId;
  }

  
  /**
   * List of unapplied transaction patterns
   **/
  
  @ApiModelProperty(required = true, value = "List of unapplied transaction patterns")
  @JsonProperty("unappliedTransactionPatterns")
  public List<TransactionPattern> getUnappliedTransactionPatterns() {
    return unappliedTransactionPatterns;
  }
  public void setUnappliedTransactionPatterns(List<TransactionPattern> unappliedTransactionPatterns) {
    this.unappliedTransactionPatterns = unappliedTransactionPatterns;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Forecast forecast = (Forecast) o;
    return Objects.equals(forecastBalance, forecast.forecastBalance) &&
        Objects.equals(accountId, forecast.accountId) &&
        Objects.equals(unappliedTransactionPatterns, forecast.unappliedTransactionPatterns);
  }

  @Override
  public int hashCode() {
    return Objects.hash(forecastBalance, accountId, unappliedTransactionPatterns);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Forecast {\n");
    
    sb.append("    forecastBalance: ").append(toIndentedString(forecastBalance)).append("\n");
    sb.append("    accountId: ").append(toIndentedString(accountId)).append("\n");
    sb.append("    unappliedTransactionPatterns: ").append(toIndentedString(unappliedTransactionPatterns)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

