package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.Amount;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class Transfer   {
  
  private String iban = null;
  private String bic = null;
  private String name = null;
  private Amount amount = null;
  private String purpose = null;
  private Long tanMediaId = null;
  private String tanScheme = null;

  
  /**
   * IBAN - International Bank Account Number (defined in ISO 13616-1)
   **/
  
  @ApiModelProperty(required = true, value = "IBAN - International Bank Account Number (defined in ISO 13616-1)")
  @JsonProperty("iban")
  public String getIban() {
    return iban;
  }
  public void setIban(String iban) {
    this.iban = iban;
  }

  
  /**
   * BIC - Business Identifier Code (defined in ISO-9362)
   **/
  
  @ApiModelProperty(value = "BIC - Business Identifier Code (defined in ISO-9362)")
  @JsonProperty("bic")
  public String getBic() {
    return bic;
  }
  public void setBic(String bic) {
    this.bic = bic;
  }

  
  /**
   * Name - Name of the creditor
   **/
  
  @ApiModelProperty(required = true, value = "Name - Name of the creditor")
  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  
  /**
   * Amount to be transfered
   **/
  
  @ApiModelProperty(required = true, value = "Amount to be transfered")
  @JsonProperty("amount")
  public Amount getAmount() {
    return amount;
  }
  public void setAmount(Amount amount) {
    this.amount = amount;
  }

  
  /**
   * Purpose
   **/
  
  @ApiModelProperty(value = "Purpose")
  @JsonProperty("purpose")
  public String getPurpose() {
    return purpose;
  }
  public void setPurpose(String purpose) {
    this.purpose = purpose;
  }

  
  /**
   * TANMediaId - The identifying ID of the TANMedia.
   **/
  
  @ApiModelProperty(value = "TANMediaId - The identifying ID of the TANMedia.")
  @JsonProperty("tanMediaId")
  public Long getTanMediaId() {
    return tanMediaId;
  }
  public void setTanMediaId(Long tanMediaId) {
    this.tanMediaId = tanMediaId;
  }

  
  /**
   * TANScheme - The scheme \"number\" that is used to verify this payment
   **/
  
  @ApiModelProperty(required = true, value = "TANScheme - The scheme \"number\" that is used to verify this payment")
  @JsonProperty("tanScheme")
  public String getTanScheme() {
    return tanScheme;
  }
  public void setTanScheme(String tanScheme) {
    this.tanScheme = tanScheme;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Transfer transfer = (Transfer) o;
    return Objects.equals(iban, transfer.iban) &&
        Objects.equals(bic, transfer.bic) &&
        Objects.equals(name, transfer.name) &&
        Objects.equals(amount, transfer.amount) &&
        Objects.equals(purpose, transfer.purpose) &&
        Objects.equals(tanMediaId, transfer.tanMediaId) &&
        Objects.equals(tanScheme, transfer.tanScheme);
  }

  @Override
  public int hashCode() {
    return Objects.hash(iban, bic, name, amount, purpose, tanMediaId, tanScheme);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Transfer {\n");
    
    sb.append("    iban: ").append(toIndentedString(iban)).append("\n");
    sb.append("    bic: ").append(toIndentedString(bic)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    purpose: ").append(toIndentedString(purpose)).append("\n");
    sb.append("    tanMediaId: ").append(toIndentedString(tanMediaId)).append("\n");
    sb.append("    tanScheme: ").append(toIndentedString(tanScheme)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

