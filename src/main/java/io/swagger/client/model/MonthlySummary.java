package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.Amount;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class MonthlySummary   {
  
  private Long id = null;
  private String month = null;
  private Long accountId = null;
  private Amount income = null;
  private Amount outgoings = null;
  private Amount balance = null;

  
  /**
   * Internal ID of this monthlySummary (generated value)
   **/
  
  @ApiModelProperty(required = true, value = "Internal ID of this monthlySummary (generated value)")
  @JsonProperty("id")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  
  /**
   * The month to which this entry belongs (year-month in ISO-8601: \"yyyy-MM\")
   **/
  
  @ApiModelProperty(required = true, value = "The month to which this entry belongs (year-month in ISO-8601: \"yyyy-MM\")")
  @JsonProperty("month")
  public String getMonth() {
    return month;
  }
  public void setMonth(String month) {
    this.month = month;
  }

  
  /**
   * ID of account to which this entry belongs
   **/
  
  @ApiModelProperty(required = true, value = "ID of account to which this entry belongs")
  @JsonProperty("accountId")
  public Long getAccountId() {
    return accountId;
  }
  public void setAccountId(Long accountId) {
    this.accountId = accountId;
  }

  
  /**
   * Sum of all incoming transactions for this month
   **/
  
  @ApiModelProperty(required = true, value = "Sum of all incoming transactions for this month")
  @JsonProperty("income")
  public Amount getIncome() {
    return income;
  }
  public void setIncome(Amount income) {
    this.income = income;
  }

  
  /**
   * Sum of all outgoing transactions for this month
   **/
  
  @ApiModelProperty(required = true, value = "Sum of all outgoing transactions for this month")
  @JsonProperty("outgoings")
  public Amount getOutgoings() {
    return outgoings;
  }
  public void setOutgoings(Amount outgoings) {
    this.outgoings = outgoings;
  }

  
  /**
   * Balance at end of month
   **/
  
  @ApiModelProperty(required = true, value = "Balance at end of month")
  @JsonProperty("balance")
  public Amount getBalance() {
    return balance;
  }
  public void setBalance(Amount balance) {
    this.balance = balance;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MonthlySummary monthlySummary = (MonthlySummary) o;
    return Objects.equals(id, monthlySummary.id) &&
        Objects.equals(month, monthlySummary.month) &&
        Objects.equals(accountId, monthlySummary.accountId) &&
        Objects.equals(income, monthlySummary.income) &&
        Objects.equals(outgoings, monthlySummary.outgoings) &&
        Objects.equals(balance, monthlySummary.balance);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, month, accountId, income, outgoings, balance);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MonthlySummary {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    month: ").append(toIndentedString(month)).append("\n");
    sb.append("    accountId: ").append(toIndentedString(accountId)).append("\n");
    sb.append("    income: ").append(toIndentedString(income)).append("\n");
    sb.append("    outgoings: ").append(toIndentedString(outgoings)).append("\n");
    sb.append("    balance: ").append(toIndentedString(balance)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

