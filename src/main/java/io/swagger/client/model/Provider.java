package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.AccessDescription;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class Provider   {
  
  private Long id = null;
  private String name = null;
  private String location = null;
  private AccessDescription accessDescription = null;
  private Boolean supported = null;
  private String type = null;

  
  /**
   * Internal ID of this provider. A constant to identify provider even when,\n for examplr, their bank code changes (provided type is BankProvider)
   **/
  
  @ApiModelProperty(required = true, value = "Internal ID of this provider. A constant to identify provider even when,\n for examplr, their bank code changes (provided type is BankProvider)")
  @JsonProperty("id")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  
  /**
   * Name of this provider (e.g., \"Hamburger Bank\")
   **/
  
  @ApiModelProperty(required = true, value = "Name of this provider (e.g., \"Hamburger Bank\")")
  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  
  /**
   * Location of this provider (e.g., \"Hamburg\")
   **/
  
  @ApiModelProperty(required = true, value = "Location of this provider (e.g., \"Hamburg\")")
  @JsonProperty("location")
  public String getLocation() {
    return location;
  }
  public void setLocation(String location) {
    this.location = location;
  }

  
  /**
   * Description of the access for the account setup (e.g., UI input fields)
   **/
  
  @ApiModelProperty(value = "Description of the access for the account setup (e.g., UI input fields)")
  @JsonProperty("accessDescription")
  public AccessDescription getAccessDescription() {
    return accessDescription;
  }
  public void setAccessDescription(AccessDescription accessDescription) {
    this.accessDescription = accessDescription;
  }

  
  /**
   * Whether this bank is supported by the AHOI API (i.e., whether you can establish a connection to this provider).
   **/
  
  @ApiModelProperty(required = true, value = "Whether this bank is supported by the AHOI API (i.e., whether you can establish a connection to this provider).")
  @JsonProperty("supported")
  public Boolean getSupported() {
    return supported;
  }
  public void setSupported(Boolean supported) {
    this.supported = supported;
  }

  
  /**
   * Discriminator for subtypes. At the moment only `BankProvider` is supported.
   **/
  
  @ApiModelProperty(required = true, value = "Discriminator for subtypes. At the moment only `BankProvider` is supported.")
  @JsonProperty("type")
  public String getType() {
    return type;
  }
  public void setType(String type) {
    this.type = type;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Provider provider = (Provider) o;
    return Objects.equals(id, provider.id) &&
        Objects.equals(name, provider.name) &&
        Objects.equals(location, provider.location) &&
        Objects.equals(accessDescription, provider.accessDescription) &&
        Objects.equals(supported, provider.supported) &&
        Objects.equals(type, provider.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, location, accessDescription, supported, type);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Provider {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    location: ").append(toIndentedString(location)).append("\n");
    sb.append("    accessDescription: ").append(toIndentedString(accessDescription)).append("\n");
    sb.append("    supported: ").append(toIndentedString(supported)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

