package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class ResponseObjectMap   {
  
  private String USERNAME = null;
  private String CUSTOMERNUMBER = null;
  private String PIN = null;
  private String TAN = null;

  
  /**
   * The username for the authorization.
   **/
  
  @ApiModelProperty(value = "The username for the authorization.")
  @JsonProperty("USERNAME")
  public String getUSERNAME() {
    return USERNAME;
  }
  public void setUSERNAME(String USERNAME) {
    this.USERNAME = USERNAME;
  }

  
  /**
   * The username for the authorization.
   **/
  
  @ApiModelProperty(value = "The username for the authorization.")
  @JsonProperty("CUSTOMERNUMBER")
  public String getCUSTOMERNUMBER() {
    return CUSTOMERNUMBER;
  }
  public void setCUSTOMERNUMBER(String CUSTOMERNUMBER) {
    this.CUSTOMERNUMBER = CUSTOMERNUMBER;
  }

  
  /**
   * The PIN for the authorization
   **/
  
  @ApiModelProperty(value = "The PIN for the authorization")
  @JsonProperty("PIN")
  public String getPIN() {
    return PIN;
  }
  public void setPIN(String PIN) {
    this.PIN = PIN;
  }

  
  /**
   * The TAN for the authorization.
   **/
  
  @ApiModelProperty(value = "The TAN for the authorization.")
  @JsonProperty("TAN")
  public String getTAN() {
    return TAN;
  }
  public void setTAN(String TAN) {
    this.TAN = TAN;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ResponseObjectMap responseObjectMap = (ResponseObjectMap) o;
    return Objects.equals(USERNAME, responseObjectMap.USERNAME) &&
        Objects.equals(CUSTOMERNUMBER, responseObjectMap.CUSTOMERNUMBER) &&
        Objects.equals(PIN, responseObjectMap.PIN) &&
        Objects.equals(TAN, responseObjectMap.TAN);
  }

  @Override
  public int hashCode() {
    return Objects.hash(USERNAME, CUSTOMERNUMBER, PIN, TAN);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ResponseObjectMap {\n");
    
    sb.append("    USERNAME: ").append(toIndentedString(USERNAME)).append("\n");
    sb.append("    CUSTOMERNUMBER: ").append(toIndentedString(CUSTOMERNUMBER)).append("\n");
    sb.append("    PIN: ").append(toIndentedString(PIN)).append("\n");
    sb.append("    TAN: ").append(toIndentedString(TAN)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

