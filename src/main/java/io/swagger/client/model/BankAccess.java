package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.Access;
import io.swagger.client.model.AccessFieldsMap;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class BankAccess extends Access  {
  
  private AccessFieldsMap accessFields = null;
  private Long id = null;
  private String type = null;
  private Long providerId = null;

  
  /**
   * Authentication data for the given provider as map of InputFieldId to String (e.g., `{\"USERNAME\", \"yourName\"}`)\n The fields necessary for the access are determined by the \n InputField descriptions of the related provider.
   **/
  
  @ApiModelProperty(required = true, value = "Authentication data for the given provider as map of InputFieldId to String (e.g., `{\"USERNAME\", \"yourName\"}`)\n The fields necessary for the access are determined by the \n InputField descriptions of the related provider.")
  @JsonProperty("accessFields")
  public AccessFieldsMap getAccessFields() {
    return accessFields;
  }
  public void setAccessFields(AccessFieldsMap accessFields) {
    this.accessFields = accessFields;
  }

  
  /**
   * Internal ID of this access (generated value)
   **/
  
  @ApiModelProperty(value = "Internal ID of this access (generated value)")
  @JsonProperty("id")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  
  /**
   * Discriminator for subtypes. At the moment only `BankAccess` is supported.
   **/
  
  @ApiModelProperty(required = true, value = "Discriminator for subtypes. At the moment only `BankAccess` is supported.")
  @JsonProperty("type")
  public String getType() {
    return type;
  }
  public void setType(String type) {
    this.type = type;
  }

  
  /**
   * Identifier of the provider to which this access belongs
   **/
  
  @ApiModelProperty(required = true, value = "Identifier of the provider to which this access belongs")
  @JsonProperty("providerId")
  public Long getProviderId() {
    return providerId;
  }
  public void setProviderId(Long providerId) {
    this.providerId = providerId;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BankAccess bankAccess = (BankAccess) o;
    return Objects.equals(accessFields, bankAccess.accessFields) &&
        Objects.equals(id, bankAccess.id) &&
        Objects.equals(type, bankAccess.type) &&
        Objects.equals(providerId, bankAccess.providerId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accessFields, id, type, providerId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BankAccess {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    accessFields: ").append(toIndentedString(accessFields)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    providerId: ").append(toIndentedString(providerId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

