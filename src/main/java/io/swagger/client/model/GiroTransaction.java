package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.Amount;
import io.swagger.client.model.Transaction;
import java.util.Date;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class GiroTransaction extends Transaction  {
  
  private Amount amount = null;
  private String purpose = null;
  private String creditorAccountNumber = null;
  private Date valueDate = null;
  private String type = null;
  private Long transactionPatternId = null;
  private String debtor = null;
  private String debtorBankCode = null;
  private Date bookingDate = null;
  private String creditor = null;
  private String creditorBankCode = null;
  private Long id = null;
  private String debtorAccountNumber = null;
  private Boolean prebooked = null;
  private String cleanPurpose = null;

  
  /**
   * Amount value
   **/
  
  @ApiModelProperty(required = true, value = "Amount value")
  @JsonProperty("amount")
  public Amount getAmount() {
    return amount;
  }
  public void setAmount(Amount amount) {
    this.amount = amount;
  }

  
  /**
   * Purpose (as given by the provider)
   **/
  
  @ApiModelProperty(required = true, value = "Purpose (as given by the provider)")
  @JsonProperty("purpose")
  public String getPurpose() {
    return purpose;
  }
  public void setPurpose(String purpose) {
    this.purpose = purpose;
  }

  
  /**
   * Creditor's account number or IBAN
   **/
  
  @ApiModelProperty(value = "Creditor's account number or IBAN")
  @JsonProperty("creditorAccountNumber")
  public String getCreditorAccountNumber() {
    return creditorAccountNumber;
  }
  public void setCreditorAccountNumber(String creditorAccountNumber) {
    this.creditorAccountNumber = creditorAccountNumber;
  }

  
  /**
   * Value Date (ISO 8601: \"yyyy-MM-dd'T'HH:mm:ssX\")
   **/
  
  @ApiModelProperty(required = true, value = "Value Date (ISO 8601: \"yyyy-MM-dd'T'HH:mm:ssX\")")
  @JsonProperty("valueDate")
  public Date getValueDate() {
    return valueDate;
  }
  public void setValueDate(Date valueDate) {
    this.valueDate = valueDate;
  }

  
  /**
   * Discriminator for subtypes. At the moment only `GiroTransaction` is supported.
   **/
  
  @ApiModelProperty(required = true, value = "Discriminator for subtypes. At the moment only `GiroTransaction` is supported.")
  @JsonProperty("type")
  public String getType() {
    return type;
  }
  public void setType(String type) {
    this.type = type;
  }

  
  /**
   * Identifier of the transactionPattern to which this transaction belongs
   **/
  
  @ApiModelProperty(value = "Identifier of the transactionPattern to which this transaction belongs")
  @JsonProperty("transactionPatternId")
  public Long getTransactionPatternId() {
    return transactionPatternId;
  }
  public void setTransactionPatternId(Long transactionPatternId) {
    this.transactionPatternId = transactionPatternId;
  }

  
  /**
   * Debtor's name
   **/
  
  @ApiModelProperty(required = true, value = "Debtor's name")
  @JsonProperty("debtor")
  public String getDebtor() {
    return debtor;
  }
  public void setDebtor(String debtor) {
    this.debtor = debtor;
  }

  
  /**
   * Debtor's bank code or BIC
   **/
  
  @ApiModelProperty(value = "Debtor's bank code or BIC")
  @JsonProperty("debtorBankCode")
  public String getDebtorBankCode() {
    return debtorBankCode;
  }
  public void setDebtorBankCode(String debtorBankCode) {
    this.debtorBankCode = debtorBankCode;
  }

  
  /**
   * Booking date (ISO 8601: \"yyyy-MM-dd'T'HH:mm:ssX\")
   **/
  
  @ApiModelProperty(required = true, value = "Booking date (ISO 8601: \"yyyy-MM-dd'T'HH:mm:ssX\")")
  @JsonProperty("bookingDate")
  public Date getBookingDate() {
    return bookingDate;
  }
  public void setBookingDate(Date bookingDate) {
    this.bookingDate = bookingDate;
  }

  
  /**
   * Creditor's name
   **/
  
  @ApiModelProperty(required = true, value = "Creditor's name")
  @JsonProperty("creditor")
  public String getCreditor() {
    return creditor;
  }
  public void setCreditor(String creditor) {
    this.creditor = creditor;
  }

  
  /**
   * Creditor's bank code or BIC
   **/
  
  @ApiModelProperty(value = "Creditor's bank code or BIC")
  @JsonProperty("creditorBankCode")
  public String getCreditorBankCode() {
    return creditorBankCode;
  }
  public void setCreditorBankCode(String creditorBankCode) {
    this.creditorBankCode = creditorBankCode;
  }

  
  /**
   * Internal ID of this transaction (generated value)
   **/
  
  @ApiModelProperty(required = true, value = "Internal ID of this transaction (generated value)")
  @JsonProperty("id")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  
  /**
   * Debtor's account number or IBAN
   **/
  
  @ApiModelProperty(value = "Debtor's account number or IBAN")
  @JsonProperty("debtorAccountNumber")
  public String getDebtorAccountNumber() {
    return debtorAccountNumber;
  }
  public void setDebtorAccountNumber(String debtorAccountNumber) {
    this.debtorAccountNumber = debtorAccountNumber;
  }

  
  /**
   * Flag to identify if the transaction is marked as pre-booked
   **/
  
  @ApiModelProperty(required = true, value = "Flag to identify if the transaction is marked as pre-booked")
  @JsonProperty("prebooked")
  public Boolean getPrebooked() {
    return prebooked;
  }
  public void setPrebooked(Boolean prebooked) {
    this.prebooked = prebooked;
  }

  
  /**
   * Cleaned purpose. Some SEPA information is filtered out (e.g., \"KREF+-1434947533-2...\")
   **/
  
  @ApiModelProperty(value = "Cleaned purpose. Some SEPA information is filtered out (e.g., \"KREF+-1434947533-2...\")")
  @JsonProperty("cleanPurpose")
  public String getCleanPurpose() {
    return cleanPurpose;
  }
  public void setCleanPurpose(String cleanPurpose) {
    this.cleanPurpose = cleanPurpose;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GiroTransaction giroTransaction = (GiroTransaction) o;
    return Objects.equals(amount, giroTransaction.amount) &&
        Objects.equals(purpose, giroTransaction.purpose) &&
        Objects.equals(creditorAccountNumber, giroTransaction.creditorAccountNumber) &&
        Objects.equals(valueDate, giroTransaction.valueDate) &&
        Objects.equals(type, giroTransaction.type) &&
        Objects.equals(transactionPatternId, giroTransaction.transactionPatternId) &&
        Objects.equals(debtor, giroTransaction.debtor) &&
        Objects.equals(debtorBankCode, giroTransaction.debtorBankCode) &&
        Objects.equals(bookingDate, giroTransaction.bookingDate) &&
        Objects.equals(creditor, giroTransaction.creditor) &&
        Objects.equals(creditorBankCode, giroTransaction.creditorBankCode) &&
        Objects.equals(id, giroTransaction.id) &&
        Objects.equals(debtorAccountNumber, giroTransaction.debtorAccountNumber) &&
        Objects.equals(prebooked, giroTransaction.prebooked) &&
        Objects.equals(cleanPurpose, giroTransaction.cleanPurpose);
  }

  @Override
  public int hashCode() {
    return Objects.hash(amount, purpose, creditorAccountNumber, valueDate, type, transactionPatternId, debtor, debtorBankCode, bookingDate, creditor, creditorBankCode, id, debtorAccountNumber, prebooked, cleanPurpose);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GiroTransaction {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    purpose: ").append(toIndentedString(purpose)).append("\n");
    sb.append("    creditorAccountNumber: ").append(toIndentedString(creditorAccountNumber)).append("\n");
    sb.append("    valueDate: ").append(toIndentedString(valueDate)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    transactionPatternId: ").append(toIndentedString(transactionPatternId)).append("\n");
    sb.append("    debtor: ").append(toIndentedString(debtor)).append("\n");
    sb.append("    debtorBankCode: ").append(toIndentedString(debtorBankCode)).append("\n");
    sb.append("    bookingDate: ").append(toIndentedString(bookingDate)).append("\n");
    sb.append("    creditor: ").append(toIndentedString(creditor)).append("\n");
    sb.append("    creditorBankCode: ").append(toIndentedString(creditorBankCode)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    debtorAccountNumber: ").append(toIndentedString(debtorAccountNumber)).append("\n");
    sb.append("    prebooked: ").append(toIndentedString(prebooked)).append("\n");
    sb.append("    cleanPurpose: ").append(toIndentedString(cleanPurpose)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

