package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.Account;
import io.swagger.client.model.Balance;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class BankAccount extends Account  {
  
  private String owner = null;
  private String type = null;
  private Boolean automaticRefresh = null;
  private String number = null;
  private Balance balance = null;
  private String userDefinedName = null;
  private Long providerId = null;
  private String bankCodeNumber = null;
  private String iban = null;
  private String name = null;
  private String currency = null;
  private Long id = null;
  private String bic = null;

  
  /**
   * Account owner returned by bank provider (e.g., \"Max Mustermann\")
   **/
  
  @ApiModelProperty(required = true, value = "Account owner returned by bank provider (e.g., \"Max Mustermann\")")
  @JsonProperty("owner")
  public String getOwner() {
    return owner;
  }
  public void setOwner(String owner) {
    this.owner = owner;
  }

  
  /**
   * Discriminator for subtypes. At the moment only `BankAccount` is supported.
   **/
  
  @ApiModelProperty(required = true, value = "Discriminator for subtypes. At the moment only `BankAccount` is supported.")
  @JsonProperty("type")
  public String getType() {
    return type;
  }
  public void setType(String type) {
    this.type = type;
  }

  
  /**
   * Flag to determine whether this account is automatically refreshed on the server side. \n Requires that the corresponding access has its PIN saved.\n Can be updated by using the account resource.\n\n Default is `false`.
   **/
  
  @ApiModelProperty(required = true, value = "Flag to determine whether this account is automatically refreshed on the server side. \n Requires that the corresponding access has its PIN saved.\n Can be updated by using the account resource.\n\n Default is `false`.")
  @JsonProperty("automaticRefresh")
  public Boolean getAutomaticRefresh() {
    return automaticRefresh;
  }
  public void setAutomaticRefresh(Boolean automaticRefresh) {
    this.automaticRefresh = automaticRefresh;
  }

  
  /**
   * Account number (national)
   **/
  
  @ApiModelProperty(required = true, value = "Account number (national)")
  @JsonProperty("number")
  public String getNumber() {
    return number;
  }
  public void setNumber(String number) {
    this.number = number;
  }

  
  /**
   * Current balance. This value is set whenever the account is refreshed.
   **/
  
  @ApiModelProperty(value = "Current balance. This value is set whenever the account is refreshed.")
  @JsonProperty("balance")
  public Balance getBalance() {
    return balance;
  }
  public void setBalance(Balance balance) {
    this.balance = balance;
  }

  
  /**
   * Account userDefinedName. This value can be set to define a custom name used in AHOI (e.g., \"My Giro Account\").\n Can be changed by using the account resource.
   **/
  
  @ApiModelProperty(value = "Account userDefinedName. This value can be set to define a custom name used in AHOI (e.g., \"My Giro Account\").\n Can be changed by using the account resource.")
  @JsonProperty("userDefinedName")
  public String getUserDefinedName() {
    return userDefinedName;
  }
  public void setUserDefinedName(String userDefinedName) {
    this.userDefinedName = userDefinedName;
  }

  
  /**
   * Identifier of the provider to which this account belongs
   **/
  
  @ApiModelProperty(required = true, value = "Identifier of the provider to which this account belongs")
  @JsonProperty("providerId")
  public Long getProviderId() {
    return providerId;
  }
  public void setProviderId(Long providerId) {
    this.providerId = providerId;
  }

  
  /**
   * Bank code number (BLZ, national, 8 digits)
   **/
  
  @ApiModelProperty(required = true, value = "Bank code number (BLZ, national, 8 digits)")
  @JsonProperty("bankCodeNumber")
  public String getBankCodeNumber() {
    return bankCodeNumber;
  }
  public void setBankCodeNumber(String bankCodeNumber) {
    this.bankCodeNumber = bankCodeNumber;
  }

  
  /**
   * International Bank Account Number (IBAN; ISO 13616-1)
   **/
  
  @ApiModelProperty(required = true, value = "International Bank Account Number (IBAN; ISO 13616-1)")
  @JsonProperty("iban")
  public String getIban() {
    return iban;
  }
  public void setIban(String iban) {
    this.iban = iban;
  }

  
  /**
   * Account name returned by bank provider (e.g., \"Giro Account\")
   **/
  
  @ApiModelProperty(required = true, value = "Account name returned by bank provider (e.g., \"Giro Account\")")
  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  
  /**
   * Account currency (ISO 4217) (e.g., \"EUR\")
   **/
  
  @ApiModelProperty(required = true, value = "Account currency (ISO 4217) (e.g., \"EUR\")")
  @JsonProperty("currency")
  public String getCurrency() {
    return currency;
  }
  public void setCurrency(String currency) {
    this.currency = currency;
  }

  
  /**
   * Internal ID of this account (generated value)
   **/
  
  @ApiModelProperty(required = true, value = "Internal ID of this account (generated value)")
  @JsonProperty("id")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  
  /**
   * Business Identifier Code (BIC; ISO 9362)
   **/
  
  @ApiModelProperty(required = true, value = "Business Identifier Code (BIC; ISO 9362)")
  @JsonProperty("bic")
  public String getBic() {
    return bic;
  }
  public void setBic(String bic) {
    this.bic = bic;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BankAccount bankAccount = (BankAccount) o;
    return Objects.equals(owner, bankAccount.owner) &&
        Objects.equals(type, bankAccount.type) &&
        Objects.equals(automaticRefresh, bankAccount.automaticRefresh) &&
        Objects.equals(number, bankAccount.number) &&
        Objects.equals(balance, bankAccount.balance) &&
        Objects.equals(userDefinedName, bankAccount.userDefinedName) &&
        Objects.equals(providerId, bankAccount.providerId) &&
        Objects.equals(bankCodeNumber, bankAccount.bankCodeNumber) &&
        Objects.equals(iban, bankAccount.iban) &&
        Objects.equals(name, bankAccount.name) &&
        Objects.equals(currency, bankAccount.currency) &&
        Objects.equals(id, bankAccount.id) &&
        Objects.equals(bic, bankAccount.bic);
  }

  @Override
  public int hashCode() {
    return Objects.hash(owner, type, automaticRefresh, number, balance, userDefinedName, providerId, bankCodeNumber, iban, name, currency, id, bic);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BankAccount {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    owner: ").append(toIndentedString(owner)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    automaticRefresh: ").append(toIndentedString(automaticRefresh)).append("\n");
    sb.append("    number: ").append(toIndentedString(number)).append("\n");
    sb.append("    balance: ").append(toIndentedString(balance)).append("\n");
    sb.append("    userDefinedName: ").append(toIndentedString(userDefinedName)).append("\n");
    sb.append("    providerId: ").append(toIndentedString(providerId)).append("\n");
    sb.append("    bankCodeNumber: ").append(toIndentedString(bankCodeNumber)).append("\n");
    sb.append("    iban: ").append(toIndentedString(iban)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    bic: ").append(toIndentedString(bic)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

