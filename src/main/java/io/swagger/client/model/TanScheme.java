package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.TanMedia;
import java.util.*;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class TanScheme   {
  


  public enum NameEnum {
    SMSTAN("SMSTAN"),
    ITAN("ITAN"),
    CHIPTAN("CHIPTAN"),
    PUSHTAN("PUSHTAN");

    private String value;

    NameEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return value;
    }
  }

  private NameEnum name = null;
  private List<TanMedia> tanMediaList = new ArrayList<TanMedia>();

  
  /**
   * Name of the used TAN scheme
   **/
  
  @ApiModelProperty(value = "Name of the used TAN scheme")
  @JsonProperty("name")
  public NameEnum getName() {
    return name;
  }
  public void setName(NameEnum name) {
    this.name = name;
  }

  
  /**
   * List of available TAN media for this scheme
   **/
  
  @ApiModelProperty(value = "List of available TAN media for this scheme")
  @JsonProperty("tanMediaList")
  public List<TanMedia> getTanMediaList() {
    return tanMediaList;
  }
  public void setTanMediaList(List<TanMedia> tanMediaList) {
    this.tanMediaList = tanMediaList;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TanScheme tanScheme = (TanScheme) o;
    return Objects.equals(name, tanScheme.name) &&
        Objects.equals(tanMediaList, tanScheme.tanMediaList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, tanMediaList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TanScheme {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    tanMediaList: ").append(toIndentedString(tanMediaList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

