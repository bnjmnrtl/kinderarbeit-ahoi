package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.InputFieldDescription;
import java.util.*;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class AccessDescription   {
  
  private String infoText = null;
  private List<InputFieldDescription> fieldDescriptions = new ArrayList<InputFieldDescription>();

  
  /**
   * Information text by the provider as a description for required user actions during the account setup (e.g., \n \"Provided your financial institution has not given you a separate username, please enter\n your account number as your username. Depending on your financial institution,\n additional information such as a customer number can be entered. Most of the time\n entering this information is optional.\")
   **/
  
  @ApiModelProperty(value = "Information text by the provider as a description for required user actions during the account setup (e.g., \n \"Provided your financial institution has not given you a separate username, please enter\n your account number as your username. Depending on your financial institution,\n additional information such as a customer number can be entered. Most of the time\n entering this information is optional.\")")
  @JsonProperty("infoText")
  public String getInfoText() {
    return infoText;
  }
  public void setInfoText(String infoText) {
    this.infoText = infoText;
  }

  
  /**
   * Information to describe and facilitate validation of an access.
   **/
  
  @ApiModelProperty(value = "Information to describe and facilitate validation of an access.")
  @JsonProperty("fieldDescriptions")
  public List<InputFieldDescription> getFieldDescriptions() {
    return fieldDescriptions;
  }
  public void setFieldDescriptions(List<InputFieldDescription> fieldDescriptions) {
    this.fieldDescriptions = fieldDescriptions;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AccessDescription accessDescription = (AccessDescription) o;
    return Objects.equals(infoText, accessDescription.infoText) &&
        Objects.equals(fieldDescriptions, accessDescription.fieldDescriptions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(infoText, fieldDescriptions);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AccessDescription {\n");
    
    sb.append("    infoText: ").append(toIndentedString(infoText)).append("\n");
    sb.append("    fieldDescriptions: ").append(toIndentedString(fieldDescriptions)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

