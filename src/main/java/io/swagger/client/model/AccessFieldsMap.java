package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class AccessFieldsMap   {
  
  private String USERNAME = null;
  private String CUSTOMERNUMBER = null;
  private String PIN = null;

  
  /**
   * Should be filled with the username if the Provider object signals this as mandatory.
   **/
  
  @ApiModelProperty(value = "Should be filled with the username if the Provider object signals this as mandatory.")
  @JsonProperty("USERNAME")
  public String getUSERNAME() {
    return USERNAME;
  }
  public void setUSERNAME(String USERNAME) {
    this.USERNAME = USERNAME;
  }

  
  /**
   * Should be filled with the customer number if the Provider object signals this as mandatory.
   **/
  
  @ApiModelProperty(value = "Should be filled with the customer number if the Provider object signals this as mandatory.")
  @JsonProperty("CUSTOMERNUMBER")
  public String getCUSTOMERNUMBER() {
    return CUSTOMERNUMBER;
  }
  public void setCUSTOMERNUMBER(String CUSTOMERNUMBER) {
    this.CUSTOMERNUMBER = CUSTOMERNUMBER;
  }

  
  /**
   * Should be filled with the PIN if the Provider object signals this as mandatory.
   **/
  
  @ApiModelProperty(value = "Should be filled with the PIN if the Provider object signals this as mandatory.")
  @JsonProperty("PIN")
  public String getPIN() {
    return PIN;
  }
  public void setPIN(String PIN) {
    this.PIN = PIN;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AccessFieldsMap accessFieldsMap = (AccessFieldsMap) o;
    return Objects.equals(USERNAME, accessFieldsMap.USERNAME) &&
        Objects.equals(CUSTOMERNUMBER, accessFieldsMap.CUSTOMERNUMBER) &&
        Objects.equals(PIN, accessFieldsMap.PIN);
  }

  @Override
  public int hashCode() {
    return Objects.hash(USERNAME, CUSTOMERNUMBER, PIN);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AccessFieldsMap {\n");
    
    sb.append("    USERNAME: ").append(toIndentedString(USERNAME)).append("\n");
    sb.append("    CUSTOMERNUMBER: ").append(toIndentedString(CUSTOMERNUMBER)).append("\n");
    sb.append("    PIN: ").append(toIndentedString(PIN)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

