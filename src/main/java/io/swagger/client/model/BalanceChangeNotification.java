package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.Amount;
import io.swagger.client.model.Notification;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class BalanceChangeNotification extends Notification  {
  
  private Long accountId = null;
  private Amount upperThreshold = null;
  private Long id = null;
  private String type = null;
  private Amount lowerThreshold = null;

  
  /**
   * Identifier of the account to which this notification belongs.
   **/
  
  @ApiModelProperty(required = true, value = "Identifier of the account to which this notification belongs.")
  @JsonProperty("accountId")
  public Long getAccountId() {
    return accountId;
  }
  public void setAccountId(Long accountId) {
    this.accountId = accountId;
  }

  
  /**
   * Optional limitation; upper threshold of the amount (negative values allowed) below which notifications will be sent
   **/
  
  @ApiModelProperty(value = "Optional limitation; upper threshold of the amount (negative values allowed) below which notifications will be sent")
  @JsonProperty("upperThreshold")
  public Amount getUpperThreshold() {
    return upperThreshold;
  }
  public void setUpperThreshold(Amount upperThreshold) {
    this.upperThreshold = upperThreshold;
  }

  
  /**
   * Internal ID of this notification (generated value)
   **/
  
  @ApiModelProperty(value = "Internal ID of this notification (generated value)")
  @JsonProperty("id")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  
  /**
   * Discriminator for subtypes. At the moment `BalanceChangeNotification`, `BudgetNotification`, `DailySummaryNotification` and `NewTransactionNotification` are supported.
   **/
  
  @ApiModelProperty(required = true, value = "Discriminator for subtypes. At the moment `BalanceChangeNotification`, `BudgetNotification`, `DailySummaryNotification` and `NewTransactionNotification` are supported.")
  @JsonProperty("type")
  public String getType() {
    return type;
  }
  public void setType(String type) {
    this.type = type;
  }

  
  /**
   * Optional limitation; lower threshold of the amount (negative values allowed) above which notifications will be sent
   **/
  
  @ApiModelProperty(value = "Optional limitation; lower threshold of the amount (negative values allowed) above which notifications will be sent")
  @JsonProperty("lowerThreshold")
  public Amount getLowerThreshold() {
    return lowerThreshold;
  }
  public void setLowerThreshold(Amount lowerThreshold) {
    this.lowerThreshold = lowerThreshold;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BalanceChangeNotification balanceChangeNotification = (BalanceChangeNotification) o;
    return Objects.equals(accountId, balanceChangeNotification.accountId) &&
        Objects.equals(upperThreshold, balanceChangeNotification.upperThreshold) &&
        Objects.equals(id, balanceChangeNotification.id) &&
        Objects.equals(type, balanceChangeNotification.type) &&
        Objects.equals(lowerThreshold, balanceChangeNotification.lowerThreshold);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountId, upperThreshold, id, type, lowerThreshold);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BalanceChangeNotification {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    accountId: ").append(toIndentedString(accountId)).append("\n");
    sb.append("    upperThreshold: ").append(toIndentedString(upperThreshold)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    lowerThreshold: ").append(toIndentedString(lowerThreshold)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

