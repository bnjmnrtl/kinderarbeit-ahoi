package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class TanMedia   {
  
  private Long id = null;
  private String name = null;
  private String description = null;

  
  /**
   * Internal ID of this TanMedia (generated value)
   **/
  
  @ApiModelProperty(required = true, value = "Internal ID of this TanMedia (generated value)")
  @JsonProperty("id")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  
  /**
   * Name of TANMedia (e.g., \"iTan\")
   **/
  
  @ApiModelProperty(required = true, value = "Name of TANMedia (e.g., \"iTan\")")
  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  
  /**
   * Description of TANMedia (e.g., phone number or card number used for given type)
   **/
  
  @ApiModelProperty(required = true, value = "Description of TANMedia (e.g., phone number or card number used for given type)")
  @JsonProperty("description")
  public String getDescription() {
    return description;
  }
  public void setDescription(String description) {
    this.description = description;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TanMedia tanMedia = (TanMedia) o;
    return Objects.equals(id, tanMedia.id) &&
        Objects.equals(name, tanMedia.name) &&
        Objects.equals(description, tanMedia.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, description);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TanMedia {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

