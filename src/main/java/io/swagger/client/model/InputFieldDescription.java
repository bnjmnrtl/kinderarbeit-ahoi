package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class InputFieldDescription   {
  


  public enum IdEnum {
    USERNAME("USERNAME"),
    CUSTOMERNUMBER("CUSTOMERNUMBER"),
    PIN("PIN"),
    TAN("TAN");

    private String value;

    IdEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return value;
    }
  }

  private IdEnum id = null;
  private String label = null;
  private Boolean masked = null;


  public enum FormatEnum {
    UNSPECIFIED("UNSPECIFIED"),
    PROBABLYNUMERIC("PROBABLYNUMERIC"),
    DEFINITELYNUMERIC("DEFINITELYNUMERIC"),
    PROBABLYALPHANUMERIC("PROBABLYALPHANUMERIC"),
    DEFINITELYALPHANUMERIC("DEFINITELYALPHANUMERIC");

    private String value;

    FormatEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return value;
    }
  }

  private FormatEnum format = null;
  private Integer lengthMin = null;
  private Integer lengthMax = null;

  
  /**
   * Internal ID for this field description
   **/
  
  @ApiModelProperty(required = true, value = "Internal ID for this field description")
  @JsonProperty("id")
  public IdEnum getId() {
    return id;
  }
  public void setId(IdEnum id) {
    this.id = id;
  }

  
  /**
   * Label for this field description (e.g., 'PIN', 'Login name', 'Customer No.')
   **/
  
  @ApiModelProperty(required = true, value = "Label for this field description (e.g., 'PIN', 'Login name', 'Customer No.')")
  @JsonProperty("label")
  public String getLabel() {
    return label;
  }
  public void setLabel(String label) {
    this.label = label;
  }

  
  /**
   * Flag that indicates whether the given field value must be masked when entered on client side or encrypted when persisted\n on the server side
   **/
  
  @ApiModelProperty(required = true, value = "Flag that indicates whether the given field value must be masked when entered on client side or encrypted when persisted\n on the server side")
  @JsonProperty("masked")
  public Boolean getMasked() {
    return masked;
  }
  public void setMasked(Boolean masked) {
    this.masked = masked;
  }

  
  /**
   * Format of field value\n Can be `DEFINITELYNUMERIC` (format is definitely numeric), `DEFINITELYALPHANUMERIC` (format is definitely alphanumeric),\n `PROBABLYALPHANUMERIC` (format is probably alphanumeric; numeric is unlikely but possible), `PROBABLYNUMERIC` (format is probably\n numeric; alphanumeric is unlikely but possible) or `UNSPECIFIED` (default, no hint available)
   **/
  
  @ApiModelProperty(required = true, value = "Format of field value\n Can be `DEFINITELYNUMERIC` (format is definitely numeric), `DEFINITELYALPHANUMERIC` (format is definitely alphanumeric),\n `PROBABLYALPHANUMERIC` (format is probably alphanumeric; numeric is unlikely but possible), `PROBABLYNUMERIC` (format is probably\n numeric; alphanumeric is unlikely but possible) or `UNSPECIFIED` (default, no hint available)")
  @JsonProperty("format")
  public FormatEnum getFormat() {
    return format;
  }
  public void setFormat(FormatEnum format) {
    this.format = format;
  }

  
  /**
   * Minimum length of field value (0 = no limit)
   **/
  
  @ApiModelProperty(value = "Minimum length of field value (0 = no limit)")
  @JsonProperty("lengthMin")
  public Integer getLengthMin() {
    return lengthMin;
  }
  public void setLengthMin(Integer lengthMin) {
    this.lengthMin = lengthMin;
  }

  
  /**
   * Maximum length of field value (0 = no limit)
   **/
  
  @ApiModelProperty(value = "Maximum length of field value (0 = no limit)")
  @JsonProperty("lengthMax")
  public Integer getLengthMax() {
    return lengthMax;
  }
  public void setLengthMax(Integer lengthMax) {
    this.lengthMax = lengthMax;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InputFieldDescription inputFieldDescription = (InputFieldDescription) o;
    return Objects.equals(id, inputFieldDescription.id) &&
        Objects.equals(label, inputFieldDescription.label) &&
        Objects.equals(masked, inputFieldDescription.masked) &&
        Objects.equals(format, inputFieldDescription.format) &&
        Objects.equals(lengthMin, inputFieldDescription.lengthMin) &&
        Objects.equals(lengthMax, inputFieldDescription.lengthMax);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, label, masked, format, lengthMin, lengthMax);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InputFieldDescription {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    label: ").append(toIndentedString(label)).append("\n");
    sb.append("    masked: ").append(toIndentedString(masked)).append("\n");
    sb.append("    format: ").append(toIndentedString(format)).append("\n");
    sb.append("    lengthMin: ").append(toIndentedString(lengthMin)).append("\n");
    sb.append("    lengthMax: ").append(toIndentedString(lengthMax)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

