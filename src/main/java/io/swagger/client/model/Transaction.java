package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class Transaction   {
  
  private Long transactionPatternId = null;
  private Long id = null;
  private String type = null;

  
  /**
   * Identifier of the transactionPattern to which this transaction belongs
   **/
  
  @ApiModelProperty(value = "Identifier of the transactionPattern to which this transaction belongs")
  @JsonProperty("transactionPatternId")
  public Long getTransactionPatternId() {
    return transactionPatternId;
  }
  public void setTransactionPatternId(Long transactionPatternId) {
    this.transactionPatternId = transactionPatternId;
  }

  
  /**
   * Internal ID of this transaction (generated value)
   **/
  
  @ApiModelProperty(required = true, value = "Internal ID of this transaction (generated value)")
  @JsonProperty("id")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  
  /**
   * Discriminator for subtypes. At the moment only `GiroTransaction` is supported.
   **/
  
  @ApiModelProperty(required = true, value = "Discriminator for subtypes. At the moment only `GiroTransaction` is supported.")
  @JsonProperty("type")
  public String getType() {
    return type;
  }
  public void setType(String type) {
    this.type = type;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Transaction transaction = (Transaction) o;
    return Objects.equals(transactionPatternId, transaction.transactionPatternId) &&
        Objects.equals(id, transaction.id) &&
        Objects.equals(type, transaction.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(transactionPatternId, id, type);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Transaction {\n");
    
    sb.append("    transactionPatternId: ").append(toIndentedString(transactionPatternId)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

