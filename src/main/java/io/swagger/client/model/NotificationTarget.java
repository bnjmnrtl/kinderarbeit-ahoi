package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class NotificationTarget   {
  
  private Long id = null;
  private String appToken = null;
  private String productId = null;


  public enum OperatingSystemEnum {
    IOS("IOS"),
    ANDROID("ANDROID");

    private String value;

    OperatingSystemEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return value;
    }
  }

  private OperatingSystemEnum operatingSystem = null;


  public enum StateEnum {
    OK("OK"),
    UNDEFINED("UNDEFINED"),
    TARGET_UNKNOWN("TARGET_UNKNOWN"),
    INVALID_TOKEN_FORMAT("INVALID_TOKEN_FORMAT");

    private String value;

    StateEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return value;
    }
  }

  private StateEnum state = null;
  private String locale = null;

  
  /**
   * Internal ID of this notificationTarget (generated value)
   **/
  
  @ApiModelProperty(value = "Internal ID of this notificationTarget (generated value)")
  @JsonProperty("id")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  
  /**
   * Installation of specific application token to which to send push notifications. (e.g., device token on iOS devices)
   **/
  
  @ApiModelProperty(required = true, value = "Installation of specific application token to which to send push notifications. (e.g., device token on iOS devices)")
  @JsonProperty("appToken")
  public String getAppToken() {
    return appToken;
  }
  public void setAppToken(String appToken) {
    this.appToken = appToken;
  }

  
  /**
   * ID of the application. Has to be set up in AHOI. Use \"sandbox-product\" in sandbox environment.
   **/
  
  @ApiModelProperty(required = true, value = "ID of the application. Has to be set up in AHOI. Use \"sandbox-product\" in sandbox environment.")
  @JsonProperty("productId")
  public String getProductId() {
    return productId;
  }
  public void setProductId(String productId) {
    this.productId = productId;
  }

  
  /**
   * Operating system of the application
   **/
  
  @ApiModelProperty(required = true, value = "Operating system of the application")
  @JsonProperty("operatingSystem")
  public OperatingSystemEnum getOperatingSystem() {
    return operatingSystem;
  }
  public void setOperatingSystem(OperatingSystemEnum operatingSystem) {
    this.operatingSystem = operatingSystem;
  }

  
  /**
   * State of the application
   **/
  
  @ApiModelProperty(value = "State of the application")
  @JsonProperty("state")
  public StateEnum getState() {
    return state;
  }
  public void setState(StateEnum state) {
    this.state = state;
  }

  
  /**
   * Locale used to determine notification titles and texts for this target. \n Defaults to 'de_DE'.
   **/
  
  @ApiModelProperty(required = true, value = "Locale used to determine notification titles and texts for this target. \n Defaults to 'de_DE'.")
  @JsonProperty("locale")
  public String getLocale() {
    return locale;
  }
  public void setLocale(String locale) {
    this.locale = locale;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NotificationTarget notificationTarget = (NotificationTarget) o;
    return Objects.equals(id, notificationTarget.id) &&
        Objects.equals(appToken, notificationTarget.appToken) &&
        Objects.equals(productId, notificationTarget.productId) &&
        Objects.equals(operatingSystem, notificationTarget.operatingSystem) &&
        Objects.equals(state, notificationTarget.state) &&
        Objects.equals(locale, notificationTarget.locale);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, appToken, productId, operatingSystem, state, locale);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NotificationTarget {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    appToken: ").append(toIndentedString(appToken)).append("\n");
    sb.append("    productId: ").append(toIndentedString(productId)).append("\n");
    sb.append("    operatingSystem: ").append(toIndentedString(operatingSystem)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    locale: ").append(toIndentedString(locale)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

