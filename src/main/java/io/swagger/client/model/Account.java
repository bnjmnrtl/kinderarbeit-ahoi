package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class Account   {
  
  private Long id = null;
  private String name = null;
  private String userDefinedName = null;
  private String owner = null;
  private Long providerId = null;


  public enum KindEnum {
    GIRO("GIRO"),
    SPAR("SPAR"),
    FESTGELD("FESTGELD"),
    DEPOT("DEPOT"),
    DARLEHEN("DARLEHEN"),
    KREDITKARTE("KREDITKARTE"),
    BAUSPAR("BAUSPAR"),
    VL_SPAR("VL_SPAR"),
    VL_BAUSPAR("VL_BAUSPAR"),
    VL_WERTPAPIERSPARVERTRAG("VL_WERTPAPIERSPARVERTRAG");

    private String value;

    KindEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return value;
    }
  }

  private KindEnum kind = null;
  private Boolean automaticRefresh = null;
  private String type = null;

  
  /**
   * Internal ID of this account (generated value)
   **/
  
  @ApiModelProperty(required = true, value = "Internal ID of this account (generated value)")
  @JsonProperty("id")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  
  /**
   * Account name returned by bank provider (e.g., \"Giro Account\")
   **/
  
  @ApiModelProperty(required = true, value = "Account name returned by bank provider (e.g., \"Giro Account\")")
  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  
  /**
   * Account userDefinedName. This value can be set to define a custom name used in AHOI (e.g., \"My Giro Account\").\n Can be changed by using the account resource.
   **/
  
  @ApiModelProperty(value = "Account userDefinedName. This value can be set to define a custom name used in AHOI (e.g., \"My Giro Account\").\n Can be changed by using the account resource.")
  @JsonProperty("userDefinedName")
  public String getUserDefinedName() {
    return userDefinedName;
  }
  public void setUserDefinedName(String userDefinedName) {
    this.userDefinedName = userDefinedName;
  }

  
  /**
   * Account owner returned by bank provider (e.g., \"Max Mustermann\")
   **/
  
  @ApiModelProperty(required = true, value = "Account owner returned by bank provider (e.g., \"Max Mustermann\")")
  @JsonProperty("owner")
  public String getOwner() {
    return owner;
  }
  public void setOwner(String owner) {
    this.owner = owner;
  }

  
  /**
   * Identifier of the provider to which this account belongs
   **/
  
  @ApiModelProperty(required = true, value = "Identifier of the provider to which this account belongs")
  @JsonProperty("providerId")
  public Long getProviderId() {
    return providerId;
  }
  public void setProviderId(Long providerId) {
    this.providerId = providerId;
  }

  
  /**
   * An account kind is a classification of its structure and its possibilities. \n This is typically defined by the bank provider.
   **/
  
  @ApiModelProperty(required = true, value = "An account kind is a classification of its structure and its possibilities. \n This is typically defined by the bank provider.")
  @JsonProperty("kind")
  public KindEnum getKind() {
    return kind;
  }
  public void setKind(KindEnum kind) {
    this.kind = kind;
  }

  
  /**
   * Flag to determine whether this account is automatically refreshed on the server side. \n Requires that the corresponding access has its PIN saved.\n Can be updated by using the account resource.\n\n Default is `false`.
   **/
  
  @ApiModelProperty(required = true, value = "Flag to determine whether this account is automatically refreshed on the server side. \n Requires that the corresponding access has its PIN saved.\n Can be updated by using the account resource.\n\n Default is `false`.")
  @JsonProperty("automaticRefresh")
  public Boolean getAutomaticRefresh() {
    return automaticRefresh;
  }
  public void setAutomaticRefresh(Boolean automaticRefresh) {
    this.automaticRefresh = automaticRefresh;
  }

  
  /**
   * Discriminator for subtypes. At the moment only `BankAccount` is supported.
   **/
  
  @ApiModelProperty(required = true, value = "Discriminator for subtypes. At the moment only `BankAccount` is supported.")
  @JsonProperty("type")
  public String getType() {
    return type;
  }
  public void setType(String type) {
    this.type = type;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Account account = (Account) o;
    return Objects.equals(id, account.id) &&
        Objects.equals(name, account.name) &&
        Objects.equals(userDefinedName, account.userDefinedName) &&
        Objects.equals(owner, account.owner) &&
        Objects.equals(providerId, account.providerId) &&
        Objects.equals(kind, account.kind) &&
        Objects.equals(automaticRefresh, account.automaticRefresh) &&
        Objects.equals(type, account.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, userDefinedName, owner, providerId, kind, automaticRefresh, type);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Account {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    userDefinedName: ").append(toIndentedString(userDefinedName)).append("\n");
    sb.append("    owner: ").append(toIndentedString(owner)).append("\n");
    sb.append("    providerId: ").append(toIndentedString(providerId)).append("\n");
    sb.append("    kind: ").append(toIndentedString(kind)).append("\n");
    sb.append("    automaticRefresh: ").append(toIndentedString(automaticRefresh)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

