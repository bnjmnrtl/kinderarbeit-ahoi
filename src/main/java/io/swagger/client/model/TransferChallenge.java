package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.InputFieldDescription;
import io.swagger.client.model.ResponseObjectMap;
import java.util.*;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class TransferChallenge   {
  
  private Long transferTaskId = null;
  private List<InputFieldDescription> fieldDescriptions = new ArrayList<InputFieldDescription>();
  private String challenge = null;
  private ResponseObjectMap responseObjects = null;

  
  /**
   * TranferTaskID - The referenced task for that payment &mdash; challenge
   **/
  
  @ApiModelProperty(value = "TranferTaskID - The referenced task for that payment &mdash; challenge")
  @JsonProperty("transferTaskId")
  public Long getTransferTaskId() {
    return transferTaskId;
  }
  public void setTransferTaskId(Long transferTaskId) {
    this.transferTaskId = transferTaskId;
  }

  
  /**
   * Information to describe and facilitate validation of an transfer.
   **/
  
  @ApiModelProperty(value = "Information to describe and facilitate validation of an transfer.")
  @JsonProperty("fieldDescriptions")
  public List<InputFieldDescription> getFieldDescriptions() {
    return fieldDescriptions;
  }
  public void setFieldDescriptions(List<InputFieldDescription> fieldDescriptions) {
    this.fieldDescriptions = fieldDescriptions;
  }

  
  /**
   * Challenge &mdash; challenge text
   **/
  
  @ApiModelProperty(required = true, value = "Challenge &mdash; challenge text")
  @JsonProperty("challenge")
  public String getChallenge() {
    return challenge;
  }
  public void setChallenge(String challenge) {
    this.challenge = challenge;
  }

  
  /**
   * FieldSet &mdash; The required InputFields for that challenge
   **/
  
  @ApiModelProperty(required = true, value = "FieldSet &mdash; The required InputFields for that challenge")
  @JsonProperty("responseObjects")
  public ResponseObjectMap getResponseObjects() {
    return responseObjects;
  }
  public void setResponseObjects(ResponseObjectMap responseObjects) {
    this.responseObjects = responseObjects;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TransferChallenge transferChallenge = (TransferChallenge) o;
    return Objects.equals(transferTaskId, transferChallenge.transferTaskId) &&
        Objects.equals(fieldDescriptions, transferChallenge.fieldDescriptions) &&
        Objects.equals(challenge, transferChallenge.challenge) &&
        Objects.equals(responseObjects, transferChallenge.responseObjects);
  }

  @Override
  public int hashCode() {
    return Objects.hash(transferTaskId, fieldDescriptions, challenge, responseObjects);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TransferChallenge {\n");
    
    sb.append("    transferTaskId: ").append(toIndentedString(transferTaskId)).append("\n");
    sb.append("    fieldDescriptions: ").append(toIndentedString(fieldDescriptions)).append("\n");
    sb.append("    challenge: ").append(toIndentedString(challenge)).append("\n");
    sb.append("    responseObjects: ").append(toIndentedString(responseObjects)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

