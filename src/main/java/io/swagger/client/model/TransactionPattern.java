package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.Amount;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class TransactionPattern   {
  
  private Long id = null;


  public enum StateEnum {
    INACTIVE("INACTIVE"),
    ACTIVE("ACTIVE");

    private String value;

    StateEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return value;
    }
  }

  private StateEnum state = null;


  public enum CycleEnum {
    MONTHLY("MONTHLY"),
    QUARTERLY("QUARTERLY"),
    SEMI_ANNUALLY("SEMI_ANNUALLY"),
    ANNUALLY("ANNUALLY");

    private String value;

    CycleEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return value;
    }
  }

  private CycleEnum cycle = null;


  public enum OriginEnum {
    FINDER("FINDER"),
    MANUAL("MANUAL");

    private String value;

    OriginEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return value;
    }
  }

  private OriginEnum origin = null;
  private Integer day = null;
  private String relatedAccountOwner = null;
  private Amount amount = null;
  private String accountNumber = null;
  private String bankCode = null;
  private String kind = null;

  
  /**
   * Internal ID of this transactionPattern (generated value)
   **/
  
  @ApiModelProperty(value = "Internal ID of this transactionPattern (generated value)")
  @JsonProperty("id")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  
  /**
   * State of this pattern\n can be `ACTIVE` (default) or `INACTIVE` (user has disabled it)
   **/
  
  @ApiModelProperty(required = true, value = "State of this pattern\n can be `ACTIVE` (default) or `INACTIVE` (user has disabled it)")
  @JsonProperty("state")
  public StateEnum getState() {
    return state;
  }
  public void setState(StateEnum state) {
    this.state = state;
  }

  
  /**
   * Frequency of occurrence for this pattern
   **/
  
  @ApiModelProperty(required = true, value = "Frequency of occurrence for this pattern")
  @JsonProperty("cycle")
  public CycleEnum getCycle() {
    return cycle;
  }
  public void setCycle(CycleEnum cycle) {
    this.cycle = cycle;
  }

  
  /**
   * Origin of creation can be `FINDER` (automatically found) or `MANUAL` (created by user). Cannot be set with creation or update.
   **/
  
  @ApiModelProperty(value = "Origin of creation can be `FINDER` (automatically found) or `MANUAL` (created by user). Cannot be set with creation or update.")
  @JsonProperty("origin")
  public OriginEnum getOrigin() {
    return origin;
  }
  public void setOrigin(OriginEnum origin) {
    this.origin = origin;
  }

  
  /**
   * Day in the cycle this pattern occurs
   **/
  
  @ApiModelProperty(required = true, value = "Day in the cycle this pattern occurs")
  @JsonProperty("day")
  public Integer getDay() {
    return day;
  }
  public void setDay(Integer day) {
    this.day = day;
  }

  
  /**
   * Name of owner of related account (debtor or creditor)
   **/
  
  @ApiModelProperty(required = true, value = "Name of owner of related account (debtor or creditor)")
  @JsonProperty("relatedAccountOwner")
  public String getRelatedAccountOwner() {
    return relatedAccountOwner;
  }
  public void setRelatedAccountOwner(String relatedAccountOwner) {
    this.relatedAccountOwner = relatedAccountOwner;
  }

  
  /**
   * Amount value
   **/
  
  @ApiModelProperty(required = true, value = "Amount value")
  @JsonProperty("amount")
  public Amount getAmount() {
    return amount;
  }
  public void setAmount(Amount amount) {
    this.amount = amount;
  }

  
  /**
   * Account number or IBAN of related account (debtor or creditor)
   **/
  
  @ApiModelProperty(required = true, value = "Account number or IBAN of related account (debtor or creditor)")
  @JsonProperty("accountNumber")
  public String getAccountNumber() {
    return accountNumber;
  }
  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  
  /**
   * Bank code number or BIC of related account (debtor or creditor)
   **/
  
  @ApiModelProperty(required = true, value = "Bank code number or BIC of related account (debtor or creditor)")
  @JsonProperty("bankCode")
  public String getBankCode() {
    return bankCode;
  }
  public void setBankCode(String bankCode) {
    this.bankCode = bankCode;
  }

  
  /**
   * Kind of transaction (e.g., \"Lastschrift\" or \"Dauerauftrag\")
   **/
  
  @ApiModelProperty(value = "Kind of transaction (e.g., \"Lastschrift\" or \"Dauerauftrag\")")
  @JsonProperty("kind")
  public String getKind() {
    return kind;
  }
  public void setKind(String kind) {
    this.kind = kind;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TransactionPattern transactionPattern = (TransactionPattern) o;
    return Objects.equals(id, transactionPattern.id) &&
        Objects.equals(state, transactionPattern.state) &&
        Objects.equals(cycle, transactionPattern.cycle) &&
        Objects.equals(origin, transactionPattern.origin) &&
        Objects.equals(day, transactionPattern.day) &&
        Objects.equals(relatedAccountOwner, transactionPattern.relatedAccountOwner) &&
        Objects.equals(amount, transactionPattern.amount) &&
        Objects.equals(accountNumber, transactionPattern.accountNumber) &&
        Objects.equals(bankCode, transactionPattern.bankCode) &&
        Objects.equals(kind, transactionPattern.kind);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, state, cycle, origin, day, relatedAccountOwner, amount, accountNumber, bankCode, kind);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TransactionPattern {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    cycle: ").append(toIndentedString(cycle)).append("\n");
    sb.append("    origin: ").append(toIndentedString(origin)).append("\n");
    sb.append("    day: ").append(toIndentedString(day)).append("\n");
    sb.append("    relatedAccountOwner: ").append(toIndentedString(relatedAccountOwner)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    accountNumber: ").append(toIndentedString(accountNumber)).append("\n");
    sb.append("    bankCode: ").append(toIndentedString(bankCode)).append("\n");
    sb.append("    kind: ").append(toIndentedString(kind)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

