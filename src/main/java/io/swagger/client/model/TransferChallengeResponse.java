package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.ResponseObjectMap;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class TransferChallengeResponse   {
  
  private Long transferTaskId = null;
  private ResponseObjectMap responseObjects = null;

  
  /**
   * TransferTaskID &mdash; The referenced task for that payment challenge
   **/
  
  @ApiModelProperty(value = "TransferTaskID &mdash; The referenced task for that payment challenge")
  @JsonProperty("transferTaskId")
  public Long getTransferTaskId() {
    return transferTaskId;
  }
  public void setTransferTaskId(Long transferTaskId) {
    this.transferTaskId = transferTaskId;
  }

  
  /**
   * FieldSet &mdash; The required InputFields for that challenge
   **/
  
  @ApiModelProperty(required = true, value = "FieldSet &mdash; The required InputFields for that challenge")
  @JsonProperty("responseObjects")
  public ResponseObjectMap getResponseObjects() {
    return responseObjects;
  }
  public void setResponseObjects(ResponseObjectMap responseObjects) {
    this.responseObjects = responseObjects;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TransferChallengeResponse transferChallengeResponse = (TransferChallengeResponse) o;
    return Objects.equals(transferTaskId, transferChallengeResponse.transferTaskId) &&
        Objects.equals(responseObjects, transferChallengeResponse.responseObjects);
  }

  @Override
  public int hashCode() {
    return Objects.hash(transferTaskId, responseObjects);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TransferChallengeResponse {\n");
    
    sb.append("    transferTaskId: ").append(toIndentedString(transferTaskId)).append("\n");
    sb.append("    responseObjects: ").append(toIndentedString(responseObjects)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

