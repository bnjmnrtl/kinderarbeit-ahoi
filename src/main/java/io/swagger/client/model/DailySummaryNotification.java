package io.swagger.client.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.Notification;
import java.util.*;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class DailySummaryNotification extends Notification  {
  
  private Long accountId = null;
  private Long id = null;
  private String type = null;


  public enum DaysOfWeekEnum {
    MONDAY("MONDAY"),
    TUESDAY("TUESDAY"),
    WEDNESDAY("WEDNESDAY"),
    THURSDAY("THURSDAY"),
    FRIDAY("FRIDAY"),
    SATURDAY("SATURDAY"),
    SUNDAY("SUNDAY");

    private String value;

    DaysOfWeekEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return value;
    }
  }

  private List<DaysOfWeekEnum> daysOfWeek = new ArrayList<DaysOfWeekEnum>();
  private String timeOfDay = null;

  
  /**
   * Identifier of the account to which this notification belongs
   **/
  
  @ApiModelProperty(required = true, value = "Identifier of the account to which this notification belongs")
  @JsonProperty("accountId")
  public Long getAccountId() {
    return accountId;
  }
  public void setAccountId(Long accountId) {
    this.accountId = accountId;
  }

  
  /**
   * Internal ID of this notification (generated value)
   **/
  
  @ApiModelProperty(value = "Internal ID of this notification (generated value)")
  @JsonProperty("id")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  
  /**
   * Discriminator for subtypes. At the moment `BalanceChangeNotification`, `BudgetNotification`, `DailySummaryNotification` and `NewTransactionNotification` are supported.
   **/
  
  @ApiModelProperty(required = true, value = "Discriminator for subtypes. At the moment `BalanceChangeNotification`, `BudgetNotification`, `DailySummaryNotification` and `NewTransactionNotification` are supported.")
  @JsonProperty("type")
  public String getType() {
    return type;
  }
  public void setType(String type) {
    this.type = type;
  }

  
  /**
   * The days of the week on which these notification should be triggered. All entries must be unique.\n\nCan be `MONDAY`, `TUESDAY`, `WEDNESDAY`, `THURSDAY`, `FRIDAY`, `SATURDAY` or `SUNDAY`
   **/
  
  @ApiModelProperty(required = true, value = "The days of the week on which these notification should be triggered. All entries must be unique.\n\nCan be `MONDAY`, `TUESDAY`, `WEDNESDAY`, `THURSDAY`, `FRIDAY`, `SATURDAY` or `SUNDAY`")
  @JsonProperty("daysOfWeek")
  public List<DaysOfWeekEnum> getDaysOfWeek() {
    return daysOfWeek;
  }
  public void setDaysOfWeek(List<DaysOfWeekEnum> daysOfWeek) {
    this.daysOfWeek = daysOfWeek;
  }

  
  /**
   * Time of the day when this notification should be triggered. Must be in the desired timezone or normalized to UTC. (ISO-8601; e.g.,\n \"13:45.30.123456789+02:00\")
   **/
  
  @ApiModelProperty(required = true, value = "Time of the day when this notification should be triggered. Must be in the desired timezone or normalized to UTC. (ISO-8601; e.g.,\n \"13:45.30.123456789+02:00\")")
  @JsonProperty("timeOfDay")
  public String getTimeOfDay() {
    return timeOfDay;
  }
  public void setTimeOfDay(String timeOfDay) {
    this.timeOfDay = timeOfDay;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DailySummaryNotification dailySummaryNotification = (DailySummaryNotification) o;
    return Objects.equals(accountId, dailySummaryNotification.accountId) &&
        Objects.equals(id, dailySummaryNotification.id) &&
        Objects.equals(type, dailySummaryNotification.type) &&
        Objects.equals(daysOfWeek, dailySummaryNotification.daysOfWeek) &&
        Objects.equals(timeOfDay, dailySummaryNotification.timeOfDay);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountId, id, type, daysOfWeek, timeOfDay);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DailySummaryNotification {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    accountId: ").append(toIndentedString(accountId)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    daysOfWeek: ").append(toIndentedString(daysOfWeek)).append("\n");
    sb.append("    timeOfDay: ").append(toIndentedString(timeOfDay)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

