package io.swagger.client.api;

import com.sun.jersey.api.client.GenericType;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import io.swagger.client.model.NotificationTarget;

import java.util.*;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class NotificationTargetApi {
  private ApiClient apiClient;

  public NotificationTargetApi() {
    this(Configuration.getDefaultApiClient());
  }

  public NotificationTargetApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  
  /**
   * List notification targets
   * Retrieve all notification targets for the authenticated client.
   * @return List<NotificationTarget>
   */
  public List<NotificationTarget> getNotificationTargets() throws ApiException {
    Object postBody = null;
    
    // create path and map variables
    String path = "/v2/notificationtargets".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<List<NotificationTarget>> returnType = new GenericType<List<NotificationTarget>>() {};
    return apiClient.invokeAPI(path, "GET", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Create a new notification target
   * Create a new notification target to receive configured notifications. \nThe two current supported systems are the Apple Push Notification service (APNs) for `IOS` devices and Google Cloud Messaging (GCM) for `ANDROID`, which must be supplied in **state**.\nThe **appToken** must contain the unique identifier you receive after registering your device with the messaging services.\nThe **locale** must comply to ISO 3166 language code. Currently only `de_DE` is supported.
   * @param notificationTargetDto The notification target to create
   * @return NotificationTarget
   */
  public NotificationTarget postNotificationTarget(NotificationTarget notificationTargetDto) throws ApiException {
    Object postBody = notificationTargetDto;
    
     // verify the required parameter 'notificationTargetDto' is set
     if (notificationTargetDto == null) {
        throw new ApiException(400, "Missing the required parameter 'notificationTargetDto' when calling postNotificationTarget");
     }
     
    // create path and map variables
    String path = "/v2/notificationtargets".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<NotificationTarget> returnType = new GenericType<NotificationTarget>() {};
    return apiClient.invokeAPI(path, "POST", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Get notification target
   * Retrieve notification target identified by **targetId**.
   * @param targetId The **targetId** to retrieve
   * @return NotificationTarget
   */
  public NotificationTarget getNotificationTarget(Long targetId) throws ApiException {
    Object postBody = null;
    
     // verify the required parameter 'targetId' is set
     if (targetId == null) {
        throw new ApiException(400, "Missing the required parameter 'targetId' when calling getNotificationTarget");
     }
     
    // create path and map variables
    String path = "/v2/notificationtargets/{targetId}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "targetId" + "\\}", apiClient.escapeString(targetId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<NotificationTarget> returnType = new GenericType<NotificationTarget>() {};
    return apiClient.invokeAPI(path, "GET", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Update notification target
   * Update a notification target. Usually used to change the **appToken** since the messaging services alter those periodically. The **id** in **notificationDto** must match the **targetId**.
   * @param targetId The **targetId** to update
   * @param notificationTargetDto The notification target with updated token
   * @return NotificationTarget
   */
  public NotificationTarget putNotificationTarget(Long targetId, NotificationTarget notificationTargetDto) throws ApiException {
    Object postBody = notificationTargetDto;
    
     // verify the required parameter 'targetId' is set
     if (targetId == null) {
        throw new ApiException(400, "Missing the required parameter 'targetId' when calling putNotificationTarget");
     }
     
     // verify the required parameter 'notificationTargetDto' is set
     if (notificationTargetDto == null) {
        throw new ApiException(400, "Missing the required parameter 'notificationTargetDto' when calling putNotificationTarget");
     }
     
    // create path and map variables
    String path = "/v2/notificationtargets/{targetId}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "targetId" + "\\}", apiClient.escapeString(targetId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<NotificationTarget> returnType = new GenericType<NotificationTarget>() {};
    return apiClient.invokeAPI(path, "PUT", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Delete notification target
   * Delete a notification target identified by **targetId**. Also deletes associated notifications.
   * @param targetId The **targetId** to delete
   * @return void
   */
  public void deleteNotificationTarget(Long targetId) throws ApiException {
    Object postBody = null;
    
     // verify the required parameter 'targetId' is set
     if (targetId == null) {
        throw new ApiException(400, "Missing the required parameter 'targetId' when calling deleteNotificationTarget");
     }
     
    // create path and map variables
    String path = "/v2/notificationtargets/{targetId}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "targetId" + "\\}", apiClient.escapeString(targetId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    apiClient.invokeAPI(path, "DELETE", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, null);
    
  }
  
}
