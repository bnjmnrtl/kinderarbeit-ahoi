package io.swagger.client.api;

import com.sun.jersey.api.client.GenericType;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import io.swagger.client.model.Provider;

import java.util.*;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class ProviderApi {
  private ApiClient apiClient;

  public ProviderApi() {
    this(Configuration.getDefaultApiClient());
  }

  public ProviderApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  
  /**
   * List bank providers
   * Retrieve a list of bank providers. A provider-**id** is necessary to create an _access_. To retrieve the necessary access fields, you need to query the specific `provider/{providerId}`. For performance reasons they are kept separate.
   * @param bankCode Optional &amp;mdash; if length = 8, the response will also contain data describing\n            the fields required for account setup
   * @param supported Optional &amp;mdash; response should only contain providers supported for account\n            setup via this API
   * @param query Optional &amp;mdash; search parameters for BankCode, BIC, Location, Name. Will be ignored\n            if the bankCode query parameter is set.
   * @return List<Provider>
   */
  public List<Provider> getProviders(String bankCode, Boolean supported, String query) throws ApiException {
    Object postBody = null;
    
    // create path and map variables
    String path = "/v2/providers".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    
    queryParams.addAll(apiClient.parameterToPairs("", "bankCode", bankCode));
    
    queryParams.addAll(apiClient.parameterToPairs("", "supported", supported));
    
    queryParams.addAll(apiClient.parameterToPairs("", "query", query));
    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<List<Provider>> returnType = new GenericType<List<Provider>>() {};
    return apiClient.invokeAPI(path, "GET", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Get provider
   * Retrieve a single provider identified by **providerId**.
   * @param providerId The **providerId** to retrieve
   * @return Provider
   */
  public Provider getProvider(Long providerId) throws ApiException {
    Object postBody = null;
    
     // verify the required parameter 'providerId' is set
     if (providerId == null) {
        throw new ApiException(400, "Missing the required parameter 'providerId' when calling getProvider");
     }
     
    // create path and map variables
    String path = "/v2/providers/{providerId}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "providerId" + "\\}", apiClient.escapeString(providerId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<Provider> returnType = new GenericType<Provider>() {};
    return apiClient.invokeAPI(path, "GET", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
}
