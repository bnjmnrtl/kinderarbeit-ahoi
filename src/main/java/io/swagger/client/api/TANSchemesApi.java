package io.swagger.client.api;

import com.sun.jersey.api.client.GenericType;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import io.swagger.client.model.TanScheme;

import java.util.*;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class TANSchemesApi {
  private ApiClient apiClient;

  public TANSchemesApi() {
    this(Configuration.getDefaultApiClient());
  }

  public TANSchemesApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  
  /**
   * List TAN schemes for access
   * Retrieves all available TAN schemes for access.
   * @param accessId The **id** for the access for which to retrieve the TAN list
   * @param refresh Optional &amp;mdash; refreshes the TAN scheme list. If not provided, it\n            defaults to `false`.
   * @return List<TanScheme>
   */
  public List<TanScheme> getTanSchemes(Long accessId, Boolean refresh) throws ApiException {
    Object postBody = null;
    
     // verify the required parameter 'accessId' is set
     if (accessId == null) {
        throw new ApiException(400, "Missing the required parameter 'accessId' when calling getTanSchemes");
     }
     
    // create path and map variables
    String path = "/v2/accesses/{accessId}/tanschemes".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "accessId" + "\\}", apiClient.escapeString(accessId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    
    queryParams.addAll(apiClient.parameterToPairs("", "refresh", refresh));
    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<List<TanScheme>> returnType = new GenericType<List<TanScheme>>() {};
    return apiClient.invokeAPI(path, "GET", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
}
