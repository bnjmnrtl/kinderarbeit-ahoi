package io.swagger.client.api;

import com.sun.jersey.api.client.GenericType;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import io.swagger.client.model.TransactionPattern;

import java.util.*;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class TransactionPatternApi {
  private ApiClient apiClient;

  public TransactionPatternApi() {
    this(Configuration.getDefaultApiClient());
  }

  public TransactionPatternApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  
  /**
   * List transaction patterns for account
   * Returns all transaction pattern for **accountId**. Transaction patterns are recurring transactions automatically identified by the server or manually created via [create transaction pattern](#!/Transaction_pattern/postTransactionPattern).
   * @param accountId The **accountId** for which to retrieve transaction patterns
   * @return List<TransactionPattern>
   */
  public List<TransactionPattern> listTransactionPatterns(Long accountId) throws ApiException {
    Object postBody = null;
    
     // verify the required parameter 'accountId' is set
     if (accountId == null) {
        throw new ApiException(400, "Missing the required parameter 'accountId' when calling listTransactionPatterns");
     }
     
    // create path and map variables
    String path = "/v2/accounts/{accountId}/transactionpatterns".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "accountId" + "\\}", apiClient.escapeString(accountId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<List<TransactionPattern>> returnType = new GenericType<List<TransactionPattern>>() {};
    return apiClient.invokeAPI(path, "GET", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Create a new pattern
   * Create a new pattern for an **accountId**. The **cycle** can be one of `MONTHLY`, `QUARTERLY`, `SEMI_ANNUALLY` or `ANNUALLY`. The **day** can be between `1` and `366`, depending on **cycle**:\n\n| cycle | day range | example |\n| --- | --- | --- |\n| `MONTHLY` | `1`-`31`  | every 29th of the month =&gt; `29` |\n| `QUARTERLY` | `1`-`92`  | 23rd of February (23rd of May, etc.) =&gt; `54` (31 [complete first month] + 23 [days in second month]) |\n| `SEMI_ANNUALLY` | `1`-`184` | 1st of May and 1st of November =&gt; `121` (for first half of year: 31 + 28 + 31 + 30 + 1) |\n| `ANNUALLY` | `1`-`366` | 24th of December =&gt; `358` |\n\nIf a similar pattern already exists, you will receive an HTTP status code 409.
   * @param transactionPatternDto The transaction pattern to create
   * @param accountId The **accountId** to which the new created pattern is associated with
   * @return TransactionPattern
   */
  public TransactionPattern postTransactionPattern(TransactionPattern transactionPatternDto, Long accountId) throws ApiException {
    Object postBody = transactionPatternDto;
    
     // verify the required parameter 'transactionPatternDto' is set
     if (transactionPatternDto == null) {
        throw new ApiException(400, "Missing the required parameter 'transactionPatternDto' when calling postTransactionPattern");
     }
     
     // verify the required parameter 'accountId' is set
     if (accountId == null) {
        throw new ApiException(400, "Missing the required parameter 'accountId' when calling postTransactionPattern");
     }
     
    // create path and map variables
    String path = "/v2/accounts/{accountId}/transactionpatterns".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "accountId" + "\\}", apiClient.escapeString(accountId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<TransactionPattern> returnType = new GenericType<TransactionPattern>() {};
    return apiClient.invokeAPI(path, "POST", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Get transaction pattern
   * Returns the transaction pattern identified by **patternId** in relationship with\n **accountId**.
   * @param patternId The **patternId** for the pattern to retrieve
   * @param accountId The **accoundId** for the pattern to retrieve
   * @return TransactionPattern
   */
  public TransactionPattern getTransactionPattern(Long patternId, Long accountId) throws ApiException {
    Object postBody = null;
    
     // verify the required parameter 'patternId' is set
     if (patternId == null) {
        throw new ApiException(400, "Missing the required parameter 'patternId' when calling getTransactionPattern");
     }
     
     // verify the required parameter 'accountId' is set
     if (accountId == null) {
        throw new ApiException(400, "Missing the required parameter 'accountId' when calling getTransactionPattern");
     }
     
    // create path and map variables
    String path = "/v2/accounts/{accountId}/transactionpatterns/{patternId}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "patternId" + "\\}", apiClient.escapeString(patternId.toString()))
      .replaceAll("\\{" + "accountId" + "\\}", apiClient.escapeString(accountId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<TransactionPattern> returnType = new GenericType<TransactionPattern>() {};
    return apiClient.invokeAPI(path, "GET", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Delete transaction pattern
   * Delete the transaction pattern identified by the **patternId**. All associated transactions will be updated to {{patternId = null}}.
   * @param patternId The **patternId** to delete
   * @param accountId The **accountId** for the transaction pattern to delete
   * @return void
   */
  public void deleteTransactionPattern(Long patternId, Long accountId) throws ApiException {
    Object postBody = null;
    
     // verify the required parameter 'patternId' is set
     if (patternId == null) {
        throw new ApiException(400, "Missing the required parameter 'patternId' when calling deleteTransactionPattern");
     }
     
     // verify the required parameter 'accountId' is set
     if (accountId == null) {
        throw new ApiException(400, "Missing the required parameter 'accountId' when calling deleteTransactionPattern");
     }
     
    // create path and map variables
    String path = "/v2/accounts/{accountId}/transactionpatterns/{patternId}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "patternId" + "\\}", apiClient.escapeString(patternId.toString()))
      .replaceAll("\\{" + "accountId" + "\\}", apiClient.escapeString(accountId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    apiClient.invokeAPI(path, "DELETE", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, null);
    
  }
  
  /**
   * Enable a transaction pattern
   * Disabling the transaction pattern results in ignoring the pattern in the forecast. The transaction still exists and relations with transactions are not modified.
   * @param patternId The **patternId** to (de)activate
   * @param activated If `true`, the pattern will be used for forecast calculations;\n        `false` will be ignored
   * @param accountId The **accountId** for which to activate the pattern
   * @return TransactionPattern
   */
  public TransactionPattern activateTransactionPattern(Long patternId, String activated, Long accountId) throws ApiException {
    Object postBody = null;
    
     // verify the required parameter 'patternId' is set
     if (patternId == null) {
        throw new ApiException(400, "Missing the required parameter 'patternId' when calling activateTransactionPattern");
     }
     
     // verify the required parameter 'activated' is set
     if (activated == null) {
        throw new ApiException(400, "Missing the required parameter 'activated' when calling activateTransactionPattern");
     }
     
     // verify the required parameter 'accountId' is set
     if (accountId == null) {
        throw new ApiException(400, "Missing the required parameter 'accountId' when calling activateTransactionPattern");
     }
     
    // create path and map variables
    String path = "/v2/accounts/{accountId}/transactionpatterns/{patternId}/active/{activated}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "patternId" + "\\}", apiClient.escapeString(patternId.toString()))
      .replaceAll("\\{" + "activated" + "\\}", apiClient.escapeString(activated.toString()))
      .replaceAll("\\{" + "accountId" + "\\}", apiClient.escapeString(accountId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<TransactionPattern> returnType = new GenericType<TransactionPattern>() {};
    return apiClient.invokeAPI(path, "PUT", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
}
