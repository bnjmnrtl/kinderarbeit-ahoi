package io.swagger.client.api;

import com.sun.jersey.api.client.GenericType;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import io.swagger.client.model.Transaction;

import java.util.*;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class TransactionApi {
  private ApiClient apiClient;

  public TransactionApi() {
    this(Configuration.getDefaultApiClient());
  }

  public TransactionApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  
  /**
   * List transactions for pattern
   * Retrieve all transactions for **patternId**.
   * @param patternId The **patternId** for which to retrieve transactions
   * @param accountId The **accountId** for which to retrieve transactions
   * @param maxAge Optional &amp;mdash; indicates the maximum acceptable timeframe (in seconds) since the last refresh of the given account
   * @param limit Optional &amp;mdash; limit the number of returned transactions
   * @param offset Optional &amp;mdash; skip the first ${offset} transactions in result
   * @param from Optional &amp;mdash; only return transactions with a booking date later than ${from}; an ISO8601 Month(2014-11), Date (2014-11-17) or DateTime\n        (2014-11-17T12:00:00Z)
   * @param to Optional &amp;mdash; only return transactions with a booking date prior or equal to ${to}; an ISO8601 Date, Month or DateTime
   * @return List<Transaction>
   */
  public List<Transaction> listTransactionsForPattern(Long patternId, Long accountId, Long maxAge, Integer limit, Integer offset, String from, String to) throws ApiException {
    Object postBody = null;
    
     // verify the required parameter 'patternId' is set
     if (patternId == null) {
        throw new ApiException(400, "Missing the required parameter 'patternId' when calling listTransactionsForPattern");
     }
     
     // verify the required parameter 'accountId' is set
     if (accountId == null) {
        throw new ApiException(400, "Missing the required parameter 'accountId' when calling listTransactionsForPattern");
     }
     
    // create path and map variables
    String path = "/v2/accounts/{accountId}/transactionpatterns/{patternId}/transactions".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "patternId" + "\\}", apiClient.escapeString(patternId.toString()))
      .replaceAll("\\{" + "accountId" + "\\}", apiClient.escapeString(accountId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    
    queryParams.addAll(apiClient.parameterToPairs("", "max-age", maxAge));
    
    queryParams.addAll(apiClient.parameterToPairs("", "limit", limit));
    
    queryParams.addAll(apiClient.parameterToPairs("", "offset", offset));
    
    queryParams.addAll(apiClient.parameterToPairs("", "from", from));
    
    queryParams.addAll(apiClient.parameterToPairs("", "to", to));
    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<List<Transaction>> returnType = new GenericType<List<Transaction>>() {};
    return apiClient.invokeAPI(path, "GET", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * List transactions for account
   * Retrieve all transactions for **accountId**.
   * @param accountId The **accountId** for which to retrieve transactions.
   * @param maxAge Optional &amp;mdash; indicates the maximum acceptable timeframe (in seconds) since the last refresh of the given account.
   * @param limit Optional &amp;mdash; limit the number of returned transactions
   * @param offset Optional &amp;mdash; skip the first ${offset} transactions in result
   * @param from Optional &amp;mdash; only return transactions with booking date later than ${from}; an ISO8601 Month(2014-11), Date (2014-11-17) or DateTime\n        (2014-11-17T12:00:00Z)
   * @param to Optional &amp;mdash; only return transactions with booking date prior or equal to ${to}; an ISO8601 Date, Month or DateTime
   * @return List<Transaction>
   */
  public List<Transaction> listTransactions(Long accountId, Long maxAge, Integer limit, Integer offset, String from, String to) throws ApiException {
    Object postBody = null;
    
     // verify the required parameter 'accountId' is set
     if (accountId == null) {
        throw new ApiException(400, "Missing the required parameter 'accountId' when calling listTransactions");
     }
     
    // create path and map variables
    String path = "/v2/accounts/{accountId}/transactions".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "accountId" + "\\}", apiClient.escapeString(accountId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    
    queryParams.addAll(apiClient.parameterToPairs("", "max-age", maxAge));
    
    queryParams.addAll(apiClient.parameterToPairs("", "limit", limit));
    
    queryParams.addAll(apiClient.parameterToPairs("", "offset", offset));
    
    queryParams.addAll(apiClient.parameterToPairs("", "from", from));
    
    queryParams.addAll(apiClient.parameterToPairs("", "to", to));
    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<List<Transaction>> returnType = new GenericType<List<Transaction>>() {};
    return apiClient.invokeAPI(path, "GET", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Get transaction
   * Returns the transaction identified by **transactionId** in relationship with **accountId**.
   * @param transactionId The **transactionId** for the pattern to retrieve
   * @param accountId The **accountId** for the transaction to retrieve
   * @return Transaction
   */
  public Transaction getTransaction(Long transactionId, Long accountId) throws ApiException {
    Object postBody = null;
    
     // verify the required parameter 'transactionId' is set
     if (transactionId == null) {
        throw new ApiException(400, "Missing the required parameter 'transactionId' when calling getTransaction");
     }
     
     // verify the required parameter 'accountId' is set
     if (accountId == null) {
        throw new ApiException(400, "Missing the required parameter 'accountId' when calling getTransaction");
     }
     
    // create path and map variables
    String path = "/v2/accounts/{accountId}/transactions/{transactionId}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "transactionId" + "\\}", apiClient.escapeString(transactionId.toString()))
      .replaceAll("\\{" + "accountId" + "\\}", apiClient.escapeString(accountId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<Transaction> returnType = new GenericType<Transaction>() {};
    return apiClient.invokeAPI(path, "GET", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
}
