package io.swagger.client.api;

import com.sun.jersey.api.client.GenericType;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import io.swagger.client.model.TransferChallenge;
import io.swagger.client.model.Transfer;
import io.swagger.client.model.TransferChallengeResponse;

import java.util.*;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class TransferApi {
  private ApiClient apiClient;

  public TransferApi() {
    this(Configuration.getDefaultApiClient());
  }

  public TransferApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  
  /**
   * Create a new transfer
   * The transfer represents a money\n transfer from the account identified by its ID to another bank account.\n\n **Please note:** Exception from the norm. This POST request will not return\n the two header fields X-Id and Location. Also, the returned JSON document\n does not represent the transfer entity but rather a temporary placeholder.
   * @param transfer The *Transfer* object to initiate a transfer
   * @param accountId The **accountId** for the transfer
   * @return TransferChallenge
   */
  public TransferChallenge postTransfer(Transfer transfer, Long accountId) throws ApiException {
    Object postBody = transfer;
    
     // verify the required parameter 'transfer' is set
     if (transfer == null) {
        throw new ApiException(400, "Missing the required parameter 'transfer' when calling postTransfer");
     }
     
     // verify the required parameter 'accountId' is set
     if (accountId == null) {
        throw new ApiException(400, "Missing the required parameter 'accountId' when calling postTransfer");
     }
     
    // create path and map variables
    String path = "/v2/accounts/{accountId}/transfers".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "accountId" + "\\}", apiClient.escapeString(accountId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<TransferChallenge> returnType = new GenericType<TransferChallenge>() {};
    return apiClient.invokeAPI(path, "POST", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Create a transfer authorization
   * **Please note:** Exception from the norm. This POST request will not return\n the two header fields X-Id and Location. The returned JSON document\n represents the transfer.
   * @param transferChallengeResponse The *TransferChallengeResponse* object to authorize the transfer
   * @param accountId The **account** for the triggered authorization
   * @param taskId The **taskId** given in the TransferChallenge
   * @return void
   */
  public void postAuthorization(TransferChallengeResponse transferChallengeResponse, Long accountId, Long taskId) throws ApiException {
    Object postBody = transferChallengeResponse;
    
     // verify the required parameter 'transferChallengeResponse' is set
     if (transferChallengeResponse == null) {
        throw new ApiException(400, "Missing the required parameter 'transferChallengeResponse' when calling postAuthorization");
     }
     
     // verify the required parameter 'accountId' is set
     if (accountId == null) {
        throw new ApiException(400, "Missing the required parameter 'accountId' when calling postAuthorization");
     }
     
     // verify the required parameter 'taskId' is set
     if (taskId == null) {
        throw new ApiException(400, "Missing the required parameter 'taskId' when calling postAuthorization");
     }
     
    // create path and map variables
    String path = "/v2/accounts/{accountId}/transfers/{taskId}/authorize".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "accountId" + "\\}", apiClient.escapeString(accountId.toString()))
      .replaceAll("\\{" + "taskId" + "\\}", apiClient.escapeString(taskId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    apiClient.invokeAPI(path, "POST", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, null);
    
  }
  
}
