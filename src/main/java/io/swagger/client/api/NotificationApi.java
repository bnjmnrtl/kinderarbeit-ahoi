package io.swagger.client.api;

import com.sun.jersey.api.client.GenericType;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import io.swagger.client.model.Notification;
import io.swagger.client.model.BalanceChangeNotification;
import io.swagger.client.model.BudgetNotification;
import io.swagger.client.model.DailySummaryNotification;
import io.swagger.client.model.NewTransactionNotification;

import java.util.*;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class NotificationApi {
  private ApiClient apiClient;

  public NotificationApi() {
    this(Configuration.getDefaultApiClient());
  }

  public NotificationApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  
  /**
   * List notifications
   * Retrieve all notifications associated with **targetId**.
   * @param targetId The **targetId** for which to retrieve notifications
   * @return List<Notification>
   */
  public List<Notification> getNotifications(Long targetId) throws ApiException {
    Object postBody = null;
    
     // verify the required parameter 'targetId' is set
     if (targetId == null) {
        throw new ApiException(400, "Missing the required parameter 'targetId' when calling getNotifications");
     }
     
    // create path and map variables
    String path = "/v2/notificationtargets/{targetId}/notifications".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "targetId" + "\\}", apiClient.escapeString(targetId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<List<Notification>> returnType = new GenericType<List<Notification>>() {};
    return apiClient.invokeAPI(path, "GET", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Create a balance change notification
   * Only one notification per target and account can be created.\n\n Receive notifications when the balance crosses the configured threshold; only one of **lowerThreshold** and **upperThreshold** may be set. \n\n This example sends a notification when the balance is less than 0 &amp;euro; \n\n ```json\n{\n  \&quot;type\&quot;: \&quot;BalanceChangeNotification\&quot;,\n  \&quot;accountId\&quot;: 0,\n  \&quot;upperThreshold\&quot;: {\n    \&quot;amount\&quot;: 0,\n    \&quot;currency\&quot;: \&quot;EUR\&quot;\n  }\n}\n```
   * @param targetId The **targetId** for which to create the notification
   * @param notificationDto The balance change notification to create
   * @return BalanceChangeNotification
   */
  public BalanceChangeNotification postBalanceChangeNotification(Long targetId, BalanceChangeNotification notificationDto) throws ApiException {
    Object postBody = notificationDto;
    
     // verify the required parameter 'targetId' is set
     if (targetId == null) {
        throw new ApiException(400, "Missing the required parameter 'targetId' when calling postBalanceChangeNotification");
     }
     
     // verify the required parameter 'notificationDto' is set
     if (notificationDto == null) {
        throw new ApiException(400, "Missing the required parameter 'notificationDto' when calling postBalanceChangeNotification");
     }
     
    // create path and map variables
    String path = "/v2/notificationtargets/{targetId}/notifications/balancechangenotification".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "targetId" + "\\}", apiClient.escapeString(targetId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<BalanceChangeNotification> returnType = new GenericType<BalanceChangeNotification>() {};
    return apiClient.invokeAPI(path, "POST", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Create a budget change notification
   * Receive notifications when the calculated budget for the current month crosses the configured threshold; only one of **lowerThreshold** and **upperThreshold** may be set. \n\n This example send a notification when the budget crosses 100 &amp;euro; \n\n ```json\n{\n  \&quot;type\&quot;: \&quot;BudgetNotification\&quot;,\n  \&quot;accountId\&quot;: 0,\n  \&quot;lowerThreshold\&quot;: {\n    \&quot;amount\&quot;: 10000,\n    \&quot;currency\&quot;: \&quot;EUR\&quot;\n  }\n}\n```
   * @param targetId The **targetId** for which to create the notification
   * @param notificationDto The budget change notification to create
   * @return BudgetNotification
   */
  public BudgetNotification postBudgetNotification(Long targetId, BudgetNotification notificationDto) throws ApiException {
    Object postBody = notificationDto;
    
     // verify the required parameter 'targetId' is set
     if (targetId == null) {
        throw new ApiException(400, "Missing the required parameter 'targetId' when calling postBudgetNotification");
     }
     
     // verify the required parameter 'notificationDto' is set
     if (notificationDto == null) {
        throw new ApiException(400, "Missing the required parameter 'notificationDto' when calling postBudgetNotification");
     }
     
    // create path and map variables
    String path = "/v2/notificationtargets/{targetId}/notifications/budgetnotification".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "targetId" + "\\}", apiClient.escapeString(targetId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<BudgetNotification> returnType = new GenericType<BudgetNotification>() {};
    return apiClient.invokeAPI(path, "POST", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Create a daily summary notification
   * Receive a notification for your account status on the configured days and at the configured time. Please make sure to pass your timezone or adjust for UTC. \n\n This example notifies you every day at 12:03 UTC \n\n ```json\n{\n  \&quot;type\&quot;: \&quot;DailySummaryNotification\&quot;,\n  \&quot;accountId\&quot;: 0,\n  \&quot;daysOfWeek\&quot;: [\n    &#39;MONDAY&#39;, &#39;TUESDAY&#39;, &#39;WEDNESDAY&#39;, \n    &#39;THURSDAY&#39;, &#39;FRIDAY&#39;, &#39;SATURDAY&#39;, \n    &#39;SUNDAY&#39;\n  ],\n  \&quot;timeOfDay\&quot;: \&quot;12:03Z\&quot;\n}\n```
   * @param targetId The **targetId** for which to create the notification
   * @param notificationDto The daily summary notification to create
   * @return DailySummaryNotification
   */
  public DailySummaryNotification postDailySummaryNotification(Long targetId, DailySummaryNotification notificationDto) throws ApiException {
    Object postBody = notificationDto;
    
     // verify the required parameter 'targetId' is set
     if (targetId == null) {
        throw new ApiException(400, "Missing the required parameter 'targetId' when calling postDailySummaryNotification");
     }
     
     // verify the required parameter 'notificationDto' is set
     if (notificationDto == null) {
        throw new ApiException(400, "Missing the required parameter 'notificationDto' when calling postDailySummaryNotification");
     }
     
    // create path and map variables
    String path = "/v2/notificationtargets/{targetId}/notifications/dailysummarynotification".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "targetId" + "\\}", apiClient.escapeString(targetId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<DailySummaryNotification> returnType = new GenericType<DailySummaryNotification>() {};
    return apiClient.invokeAPI(path, "POST", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Create a new transaction notification
   * Receive a notification for every new transaction, for transactions within a given threshold or that match a **searchKeyword**. To receive all transactions, simply leave the optional fields blank. \n\n  This example notifies you of every transaction that contains the keyword \&quot;food\&quot; between 20 &amp;euro; and 60 &amp;euro;: \n\n ```json\n{\n  \&quot;type\&quot;: \&quot;NewTransactionNotification\&quot;,\n  \&quot;accountId\&quot;: 0,\n  \&quot;lowerThreshold\&quot;: {\n    \&quot;amount\&quot;: 2000,\n    \&quot;currency\&quot;: \&quot;EUR\&quot;\n  },\n  \&quot;upperThreshold\&quot;: {\n    \&quot;amount\&quot;: 6000,\n    \&quot;currency\&quot;: \&quot;EUR\&quot;\n  },\n  \&quot;searchKeyword\&quot;: \&quot;Rent\&quot;\n}\n```
   * @param targetId The **targetId** for which to create the notification
   * @param notificationDto The new transaction notification to create
   * @return NewTransactionNotification
   */
  public NewTransactionNotification postNewTransactionNotification(Long targetId, NewTransactionNotification notificationDto) throws ApiException {
    Object postBody = notificationDto;
    
     // verify the required parameter 'targetId' is set
     if (targetId == null) {
        throw new ApiException(400, "Missing the required parameter 'targetId' when calling postNewTransactionNotification");
     }
     
     // verify the required parameter 'notificationDto' is set
     if (notificationDto == null) {
        throw new ApiException(400, "Missing the required parameter 'notificationDto' when calling postNewTransactionNotification");
     }
     
    // create path and map variables
    String path = "/v2/notificationtargets/{targetId}/notifications/newtransactionnotification".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "targetId" + "\\}", apiClient.escapeString(targetId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<NewTransactionNotification> returnType = new GenericType<NewTransactionNotification>() {};
    return apiClient.invokeAPI(path, "POST", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Get notification
   * Retrieve notification identified by **notificationId**.
   * @param targetId The **targetId** for which to retrieve the notification
   * @param notificationId The *notificationId* to retrieve
   * @return Notification
   */
  public Notification getNotification(Long targetId, Long notificationId) throws ApiException {
    Object postBody = null;
    
     // verify the required parameter 'targetId' is set
     if (targetId == null) {
        throw new ApiException(400, "Missing the required parameter 'targetId' when calling getNotification");
     }
     
     // verify the required parameter 'notificationId' is set
     if (notificationId == null) {
        throw new ApiException(400, "Missing the required parameter 'notificationId' when calling getNotification");
     }
     
    // create path and map variables
    String path = "/v2/notificationtargets/{targetId}/notifications/{notificationId}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "targetId" + "\\}", apiClient.escapeString(targetId.toString()))
      .replaceAll("\\{" + "notificationId" + "\\}", apiClient.escapeString(notificationId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<Notification> returnType = new GenericType<Notification>() {};
    return apiClient.invokeAPI(path, "GET", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Update notification
   * Update the notification identified by **notificationId**. The **notificationId** must match the **id** in **notificationDto**. Please note that type depending restrictions from creating a notification also apply here.
   * @param targetId The **targetId** for which to create the notification
   * @param notificationId The **notificationId** to update
   * @param notificationDto The notification data to update
   * @return Notification
   */
  public Notification putNotification(Long targetId, Long notificationId, Notification notificationDto) throws ApiException {
    Object postBody = notificationDto;
    
     // verify the required parameter 'targetId' is set
     if (targetId == null) {
        throw new ApiException(400, "Missing the required parameter 'targetId' when calling putNotification");
     }
     
     // verify the required parameter 'notificationId' is set
     if (notificationId == null) {
        throw new ApiException(400, "Missing the required parameter 'notificationId' when calling putNotification");
     }
     
     // verify the required parameter 'notificationDto' is set
     if (notificationDto == null) {
        throw new ApiException(400, "Missing the required parameter 'notificationDto' when calling putNotification");
     }
     
    // create path and map variables
    String path = "/v2/notificationtargets/{targetId}/notifications/{notificationId}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "targetId" + "\\}", apiClient.escapeString(targetId.toString()))
      .replaceAll("\\{" + "notificationId" + "\\}", apiClient.escapeString(notificationId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<Notification> returnType = new GenericType<Notification>() {};
    return apiClient.invokeAPI(path, "PUT", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Delete notification
   * Delete notification identified by **notificationId**.
   * @param targetId The **targetId** for which to delete the notification
   * @param notificationId The **notificationId** to delete
   * @return void
   */
  public void deleteNotification(Long targetId, Long notificationId) throws ApiException {
    Object postBody = null;
    
     // verify the required parameter 'targetId' is set
     if (targetId == null) {
        throw new ApiException(400, "Missing the required parameter 'targetId' when calling deleteNotification");
     }
     
     // verify the required parameter 'notificationId' is set
     if (notificationId == null) {
        throw new ApiException(400, "Missing the required parameter 'notificationId' when calling deleteNotification");
     }
     
    // create path and map variables
    String path = "/v2/notificationtargets/{targetId}/notifications/{notificationId}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "targetId" + "\\}", apiClient.escapeString(targetId.toString()))
      .replaceAll("\\{" + "notificationId" + "\\}", apiClient.escapeString(notificationId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    apiClient.invokeAPI(path, "DELETE", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, null);
    
  }
  
}
