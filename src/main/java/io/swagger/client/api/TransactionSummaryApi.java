package io.swagger.client.api;

import com.sun.jersey.api.client.GenericType;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import io.swagger.client.model.MonthlySummary;

import java.util.*;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class TransactionSummaryApi {
  private ApiClient apiClient;

  public TransactionSummaryApi() {
    this(Configuration.getDefaultApiClient());
  }

  public TransactionSummaryApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  
  /**
   * List account summaries
   * Retrieve account summaries and provide a sum for incoming and outgoing transactions.
   * @param accountId The **accountId** for which to list the summaries
   * @param limit Optional &amp;mdash; limit the number of returned summary entries
   * @param offset Optional &amp;mdash; skip the first ${offset} summary entries in the result
   * @param from Optional &amp;mdash; only return summary entries later than ${from}; an\n        ISO8601 Date (2014-11-17)
   * @param to Optional &amp;mdash; only return summary entries prior or equal to\n        ${to}; an ISO8601 Date
   * @return List<MonthlySummary>
   */
  public List<MonthlySummary> listSummary(Long accountId, Integer limit, Integer offset, String from, String to) throws ApiException {
    Object postBody = null;
    
     // verify the required parameter 'accountId' is set
     if (accountId == null) {
        throw new ApiException(400, "Missing the required parameter 'accountId' when calling listSummary");
     }
     
    // create path and map variables
    String path = "/v2/accounts/{accountId}/transactionsummaries".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "accountId" + "\\}", apiClient.escapeString(accountId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    
    queryParams.addAll(apiClient.parameterToPairs("", "limit", limit));
    
    queryParams.addAll(apiClient.parameterToPairs("", "offset", offset));
    
    queryParams.addAll(apiClient.parameterToPairs("", "from", from));
    
    queryParams.addAll(apiClient.parameterToPairs("", "to", to));
    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<List<MonthlySummary>> returnType = new GenericType<List<MonthlySummary>>() {};
    return apiClient.invokeAPI(path, "GET", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
}
