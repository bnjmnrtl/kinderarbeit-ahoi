package io.swagger.client.api;

import com.sun.jersey.api.client.GenericType;

import io.swagger.client.ApiException;
import io.swagger.client.ApiClient;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;

import io.swagger.client.model.Access;

import java.util.*;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-11-14T15:27:30.307+01:00")
public class AccessApi {
  private ApiClient apiClient;

  public AccessApi() {
    this(Configuration.getDefaultApiClient());
  }

  public AccessApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  
  /**
   * List accesses
   * Returns all registered accesses for the authenticated user. Confidential information like the PIN will not be returned.
   * @return List<Access>
   */
  public List<Access> getAccesses() throws ApiException {
    Object postBody = null;
    
    // create path and map variables
    String path = "/v2/accesses".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<List<Access>> returnType = new GenericType<List<Access>>() {};
    return apiClient.invokeAPI(path, "GET", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Create a new access
   * Create a new access and setup all associated accounts and transactions. This will also trigger the creation of monthly transaction summaries, the analysis of all accounts for recurring transactions, and the calculation of the balance forecast.\n\n If the credentials were invalid, the validation state is set accordingly. \n\n It is possible to have multiple accesses for one user.
   * @param accessDto A valid BankAccess object containing the required\n            **accessFields** as indicated by the provider object and the\n            **providerId**.
   * @return Access
   */
  public Access postAccess(Access accessDto) throws ApiException {
    Object postBody = accessDto;
    
     // verify the required parameter 'accessDto' is set
     if (accessDto == null) {
        throw new ApiException(400, "Missing the required parameter 'accessDto' when calling postAccess");
     }
     
    // create path and map variables
    String path = "/v2/accesses".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<Access> returnType = new GenericType<Access>() {};
    return apiClient.invokeAPI(path, "POST", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Get access
   * Retrieve the access with **accessId**. The retrieved object does not contain sensitive information such as the PIN.
   * @param accessId The **id** for the access to retrieve.
   * @return Access
   */
  public Access getAccess(Long accessId) throws ApiException {
    Object postBody = null;
    
     // verify the required parameter 'accessId' is set
     if (accessId == null) {
        throw new ApiException(400, "Missing the required parameter 'accessId' when calling getAccess");
     }
     
    // create path and map variables
    String path = "/v2/accesses/{accessId}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "accessId" + "\\}", apiClient.escapeString(accessId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<Access> returnType = new GenericType<Access>() {};
    return apiClient.invokeAPI(path, "GET", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Update access
   * Update the access credentials in **accessFields**. If the access does not exist, the **accessId** does not match the **id** in **accessDto** or the **providerId** is not the same, the status code 404 is returned. If another access with the same login\n data already exists, the status code 409 is returned.\n The updated access is validated by setting up an account. The status code 200 does not imply that the credentials are correct. To check this the client should obtain access.
   * @param accessId The **id** for the access to update.
   * @param accessDto The Access object that contains the changed credentials in\n            **accessFields**. Other fields cannot be edited.
   * @return Access
   */
  public Access putAccess(Long accessId, Access accessDto) throws ApiException {
    Object postBody = accessDto;
    
     // verify the required parameter 'accessId' is set
     if (accessId == null) {
        throw new ApiException(400, "Missing the required parameter 'accessId' when calling putAccess");
     }
     
     // verify the required parameter 'accessDto' is set
     if (accessDto == null) {
        throw new ApiException(400, "Missing the required parameter 'accessDto' when calling putAccess");
     }
     
    // create path and map variables
    String path = "/v2/accesses/{accessId}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "accessId" + "\\}", apiClient.escapeString(accessId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      "application/json"
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      "application/json"
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    GenericType<Access> returnType = new GenericType<Access>() {};
    return apiClient.invokeAPI(path, "PUT", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    
  }
  
  /**
   * Delete access
   * Delete access with **accessId** and all related accounts. This also deletes related notifications. If this is a user&#39;s last remaining access, all notification targets will also be deleted.
   * @param accessId The **id** for the access to delete
   * @return void
   */
  public void deleteAccess(Long accessId) throws ApiException {
    Object postBody = null;
    
     // verify the required parameter 'accessId' is set
     if (accessId == null) {
        throw new ApiException(400, "Missing the required parameter 'accessId' when calling deleteAccess");
     }
     
    // create path and map variables
    String path = "/v2/accesses/{accessId}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "accessId" + "\\}", apiClient.escapeString(accessId.toString()));

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    Map<String, String> headerParams = new HashMap<String, String>();
    Map<String, Object> formParams = new HashMap<String, Object>();

    

    

    

    final String[] accepts = {
      
    };
    final String accept = apiClient.selectHeaderAccept(accepts);

    final String[] contentTypes = {
      
    };
    final String contentType = apiClient.selectHeaderContentType(contentTypes);

    String[] authNames = new String[] { "apiKey" };

    
    apiClient.invokeAPI(path, "DELETE", queryParams, postBody, headerParams, formParams, accept, contentType, authNames, null);
    
  }
  
}
